function InputCheck(formUser){
	var formUser  = document.forms['formUser'];
	if (formUser.usernamefirst.value == "")
  {
    alert("姓氏不可为空!");
    formUser.usernamefirst.focus();
    return (false);
  }
  if (formUser.usernamelast.value == "")
  {
    alert("名字不可为空!");
    formUser.usernamelast.focus();
    return (false);
  }
  
 if (formUser.email.value == "")
  {
    alert("邮箱不可为空!");
    formUser.email.focus();
    return (false);
  }
  
  if (formUser.password.value == "")
  {
    alert("密码不可为空!");
    formUser.password.focus();
    return (false);
  }
  if (formUser.password.value.length<=5)
  {
    alert("密码不可少于6位数!");
    formUser.password.focus();
    return (false);
	
	
  }
  if (formUser.conform_password.value == "")
  {
    alert("确认密码不可为空!");
    formUser.conform_password.focus();
    return (false);
  }
  if (formUser.conform_password.value.length<=5)
  {
    alert("确认密码不可少于6位数!");
    formUser.conform_password.focus();
    return (false);
  }
  if (formUser.password.value !=formUser.conform_password.value)
  {
    alert("两次密码不一致!");
    //formUser.password2.focus();
    return (false);
  }

}