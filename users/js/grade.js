/** 
* 验证用户名是否重复的js 
* 
* @name   grade.js 
* @author jason<msn:x334@eyou.com> 
* @use    验证用户名是否存在 
* @todo 
*/ 
$(document).ready(function(){ 
    checkConfirm(); 
}); 
//验证用户名是否存在 
function checkConfirm(){ 
$("#NAME").blur(function(){ 
  var gradename  = $(this).val(); 
  var changeUrl = "GradeAdmin.php?action=check&gradename="+gradename; 
   
  $.get(changeUrl,function(str){ 
   if(str == '1'){ 
    $("#gradeInfo").html("您输入的用户名存在！请重新输入!"); 
   }else{ 
    $("#gradeInfo").html(""); 
   } 
  }) 
  return false; 
}) 
} 
