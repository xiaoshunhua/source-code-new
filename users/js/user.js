/* *
 * 修改会员信息
 */
function userEdit()
{
  var frm = document.forms['formEdit'];
  var email = frm.elements['email'].value;
  var msg = '';
  var reg = null;
  var user_security_question = frm.elements['user_security_question'] ? Utils.trim(frm.elements['user_security_question'].value) : '';
  var user_security_answer =  frm.elements['user_security_answer'] ? Utils.trim(frm.elements['user_security_answer'].value) : '';
  //alert(email);
  //alert(user_security_question);
  if (email.length == 0)
  {
    msg += "email为空" + '\n';
  }
  else
  {
    if ( ! (Utils.isEmail(email)))
    { 
      msg += "email格式错误" + '\n';
    }
  }

  if (user_security_question.length > 0 && user_security_answer == '' || document.getElementById('user_security_answer') && user_security_question.length == 0)
  {
    msg += "请填写密码问题答案" + '\n';
  }

  for (i = 7; i < frm.elements.length - 2; i++)	// 从第七项开始循环检查是否为必填项
  {
	needinput = document.getElementById(frm.elements[i].name + 'i') ? document.getElementById(frm.elements[i].name + 'i') : '';

	if (needinput != '' && frm.elements[i].value.length == 0)
	{
	  msg += '- ' + needinput.innerHTML + msg_blank + '\n';
	}
  }

  if (msg.length > 0)
  {
    alert(msg);
    return false;
  }
  else
  {
    return true;
  }
}

/* 会员修改密码 */
function editPassword()
{
  var frm = document.forms['formPassword'];
  var user_old_password     = frm.elements['user_old_password'].value;
  var user_new_password     = frm.elements['user_new_password'].value;
  var user_confirm_password = frm.elements['user_comfirm_password'].value;

  var msg = '';
  var reg = null;
  //alert(old_password);

  if (user_old_password.length == 0)
  {
    msg += "请输入您的原密码" + '\n';
  }

  if (user_new_password.length == 0)
  {
    msg += "请输入您的新密码" + '\n';
  }

  if (user_confirm_password.length == 0)
  {
    msg += "请输入您的确认密码"  + '\n';
  }

  if (user_new_password.length > 0 && user_confirm_password.length > 0)
  {
    if (user_new_password != user_confirm_password)
    {
      msg += "两次输入密码不一致"  + '\n';
    }
  }

  if (msg.length > 0)
  {
    alert(msg);
    return false;
  }
  else
  {
    return true;
  }
}

/* *
 * 对会员的留言输入作处理
 */
function submitMsg()
{
  var frm         = document.forms['formMsg'];
  var msg_title   = frm.elements['msg_title'].value;
  var msg_content = frm.elements['msg_content'].value;
  var msg = '';

  if (msg_title.length == 0)
  {
    msg += msg_title_empty + '\n';
  }
  if (msg_content.length == 0)
  {
    msg += msg_content_empty + '\n'
  }

  if (msg_title.length > 200)
  {
    msg += msg_title_limit + '\n';
  }

  if (msg.length > 0)
  {
    alert(msg);
    return false;
  }
  else
  {
    return true;
  }
}

/* *
 * 会员找回密码时，对输入作处理
 */
function submitPwdInfo()
{
  var frm = document.forms['getPassword'];
  var user_name = frm.elements['user_name'].value;
  var email     = frm.elements['email'].value;

  var errorMsg = '';
  if (user_name.length == 0)
  {
    errorMsg += user_name_empty + '\n';
  }

  if (email.length == 0)
  {
    errorMsg += email_address_empty + '\n';
  }
  else
  {
    if ( ! (Utils.isEmail(email)))
    {
      errorMsg += email_address_error + '\n';
    }
  }

  if (errorMsg.length > 0)
  {
    alert(errorMsg);
    return false;
  }

  return true;
}

/* *
 * 会员找回密码时，对输入作处理
 */
function submitPwd()
{
  var frm = document.forms['getPassword2'];
  var password = frm.elements['new_password'].value;
  var confirm_password = frm.elements['confirm_password'].value;

  var errorMsg = '';
  if (password.length == 0)
  {
    errorMsg += new_password_empty + '\n';
  }

  if (confirm_password.length == 0)
  {
    errorMsg += confirm_password_empty + '\n';
  }

  if (confirm_password != password)
  {
    errorMsg += both_password_error + '\n';
  }

  if (errorMsg.length > 0)
  {
    alert(errorMsg);
    return false;
  }
  else
  {
    return true;
  }
}

/* *
 * 处理会员提交的缺货登记
 */
function addBooking()
{
  var frm  = document.forms['formBooking'];
  var goods_id = frm.elements['id'].value;
  var rec_id  = frm.elements['rec_id'].value;
  var number  = frm.elements['number'].value;
  var desc  = frm.elements['desc'].value;
  var linkman  = frm.elements['linkman'].value;
  var email  = frm.elements['email'].value;
  var tel  = frm.elements['tel'].value;
  var msg = "";

  if (number.length == 0)
  {
    msg += booking_amount_empty + '\n';
  }
  else
  {
    var reg = /^[0-9]+/;
    if ( ! reg.test(number))
    {
      msg += booking_amount_error + '\n';
    }
  }

  if (desc.length == 0)
  {
    msg += describe_empty + '\n';
  }

  if (linkman.length == 0)
  {
    msg += contact_username_empty + '\n';
  }

  if (email.length == 0)
  {
    msg += email_empty + '\n';
  }
  else
  {
    if ( ! (Utils.isEmail(email)))
    {
      msg += email_error + '\n';
    }
  }

  if (tel.length == 0)
  {
    msg += contact_phone_empty + '\n';
  }

  if (msg.length > 0)
  {
    alert(msg);
    return false;
  }

  return true;
}

/* *
 * 会员登录
 */
function userLogin()
{
  var frm      = document.forms['formLogin'];
  var username = frm.elements['username'].value;
  //邮箱经行登陆
  var email = frm.elements['email'].value;
  var password = frm.elements['password'].value;
  var msg = '';

 /* if (username.length == 0)
  {
    msg += username_empty + '\n';
  }*/
  
  if (username.email == 0)
  {
    msg += email_empty + '\n';
  }

  if (password.length == 0)
  {
    msg += password_empty + '\n';
  }

  if (msg.length > 0)
  {
    alert(msg);
    return false;
  }
  else
  {
    return true;
  }
}

function chkstr(str)
{
  for (var i = 0; i < str.length; i++)
  {
    if (str.charCodeAt(i) < 127 && !str.substr(i,1).match(/^\w+$/ig))
    {
      return false;
    }
  }
  return true;
}

function check_password( password )
{
    if ( password.length < 6 )
    {
        //document.getElementById('password_notice').innerHTML = password_shorter;
		document.getElementById('password_notice').innerHTML = "密码少于6位数";
		//document.forms['formUser'].elements['Submit'].disabled = 'disabled';
    }
    else
    {
        //document.getElementById('password_notice').innerHTML = msg_can_rg;
		document.getElementById('password_notice').innerHTML = "密码可用";
		//document.forms['formUser'].elements['Submit'].disabled = '';
    }
}

function check_conform_password( conform_password )
{
    conform_password = document.getElementById('conform_password').value;
	password = document.getElementById('password').value;
    
    if ( conform_password.length < 6 )
    {
        //document.getElementById('conform_password_notice').innerHTML = password_shorter;
		document.getElementById('conform_password_notice').innerHTML ="密码少于6位数";
        return false;
    }
    if ( conform_password != password )
    {
        //document.getElementById('conform_password_notice').innerHTML = confirm_password_invalid;
		document.getElementById('conform_password_notice').innerHTML = "两次密码不一致";
    }
    else
    {
        //document.getElementById('conform_password_notice').innerHTML = msg_can_rg;
		document.getElementById('conform_password_notice').innerHTML = "密码可用";
    }
}


function is_registeredusernamefirst( usernamefirst )
{
	var usernamefirst = document.getElementById('usernamefirst').value;
    var submit_disabled = false;
	var unlen = usernamefirst.replace(/[^\x00-\xff]/g, "**").length;

    if ( usernamefirst == '' )
    {
        //document.getElementById('usernamefirst_notice').innerHTML = msg_un_blank_usernamefirst;
		document.getElementById('usernamefirst_notice').innerHTML = "姓氏不能为空";
        var submit_disabled = true;
    }

    if ( !chkstr( usernamefirst ) )
    {
        document.getElementById('usernamefirst_notice').innerHTML = msg_un_format_usernamefirst;
		
        var submit_disabled = true;
    }
    if ( unlen < 2 && unlen>0)
    { 
        //document.getElementById('usernamefirst_notice').innerHTML = usernamefirst_shorter;
		document.getElementById('usernamefirst_notice').innerHTML = "姓氏不小于1个字符";
        var submit_disabled = true;
    }
    if ( unlen > 4 )
    {
        //document.getElementById('usernamefirst_notice').innerHTML = msg_un_length_usernamefirst;
		document.getElementById('usernamefirst_notice').innerHTML = "姓氏不大于4个字符";
        var submit_disabled = true;
    }
    if ( submit_disabled )
    {
        document.forms['formUser'].elements['Submit'].disabled = 'disabled';
        return false;
    }
	else
	{
		document.getElementById('usernamefirst_notice').innerHTML = "姓氏可用";
    }
    //Ajax.call( 'user.php?act=is_registered', 'usernamefirst=' + usernamefirst, registed_callback , 'GET', 'TEXT', true, true );
}



function is_registeredusernamelast( usernamelast )
{
	var usernamelast = document.getElementById('usernamelast').value;
    var submit_disabled = false;
	var unlen = usernamelast.replace(/[^\x00-\xff]/g, "**").length;

    if ( usernamelast == '' )
    {
        //document.getElementById('usernamelast_notice').innerHTML = msg_un_blank;
		document.getElementById('usernamelast_notice').innerHTML = "名字不能为空";
        var submit_disabled = true;
    }

    if ( !chkstr( usernamelast ) )
    {
        document.getElementById('usernamelast_notice').innerHTML = msg_un_format;
        var submit_disabled = true;
    }
    if ( unlen < 2 && unlen>0)
    { 
        //document.getElementById('usernamelast_notice').innerHTML = usernamelast_shorter;
		document.getElementById('usernamelast_notice').innerHTML = "名字不能小于1个字符";
        var submit_disabled = true;
    }
    if ( unlen > 14 )
    {
        //document.getElementById('usernamelast_notice').innerHTML = msg_un_length;
		document.getElementById('usernamelast_notice').innerHTML = "名字不能超过14个字符";
        var submit_disabled = true;
    }
    if ( submit_disabled )
    {
        document.forms['formUser'].elements['Submit'].disabled = 'disabled';
        return false;
    }
	
	else
	{
		document.getElementById('usernamelast_notice').innerHTML = "名字可用";
    }
    //Ajax.call( 'sign_up.php?act=is_registered', 'usernamelast=' + usernamelast, registed_callback2 , 'GET', 'TEXT', true, true );
}



function registed_callback(result)
{
  if ( result == "true" )
  {
    document.getElementById('usernamefirst_notice').innerHTML = msg_can_rg;
    document.forms['formUser'].elements['Submit'].disabled = '';
  }
  else
  {
    document.getElementById('usernamefirst_notice').innerHTML = msg_un_registered;
    document.forms['formUser'].elements['Submit'].disabled = 'disabled';
  }
}

function registed_callback2(result)
{
  if ( result == "true" )
  {
    document.getElementById('usernamelast_notice').innerHTML = "可以注册";
    document.forms['formUser'].elements['Submit'].disabled = '';
  }
  else
  {
    document.getElementById('usernamelast_notice').innerHTML = "不能注册";
    document.forms['formUser'].elements['Submit'].disabled = 'disabled';
  }
}


function checkEmail(email)
{
  var submit_disabled = false;
  
  if (email == '')
  {
    //document.getElementById('email_notice').innerHTML = msg_email_blank;
	document.getElementById('email_notice').innerHTML = "email不能为空";
    submit_disabled = true;
  }
  else if (!Utils.isEmail(email))
  {
    //document.getElementById('email_notice').innerHTML = msg_email_format;
	document.getElementById('email_notice').innerHTML = "email格式不对";
    submit_disabled = true;
  }
 
  if( submit_disabled )
  {
    document.forms['formUser'].elements['Submit'].disabled = 'disabled';
    return false;
  }
  //alert(email);
  
  //Ajax.call( 'user.php?act=check_email', 'email=' + email, check_email_callback , 'GET', 'TEXT', true, true );
  Ajax.call( 'sign_up_yz.php?act=check_email', 'email=' + email, check_email_callback, 'GET', 'TEXT', true, true );
}

/*****
*登录时候验证
**/

function checkEmail_login_up(email)
{
  var submit_disabled = false;
  
  if (email == '')
  {
    //document.getElementById('email_notice').innerHTML = msg_email_blank;
	document.getElementById('email_notice').innerHTML = "email不能为空";
    submit_disabled = true;
  }
  else if (!Utils.isEmail(email))
  {
    //document.getElementById('email_notice').innerHTML = msg_email_format;
	document.getElementById('email_notice').innerHTML = "email格式不对";
    submit_disabled = true;
  }
 
  if( submit_disabled )
  {
    document.forms['formUser'].elements['Submit'].disabled = 'disabled';
    return false;
  }
  //alert(email);
  
  //Ajax.call( 'user.php?act=check_email', 'email=' + email, check_email_callback , 'GET', 'TEXT', true, true );
  Ajax.call( 'sign_up_yz.php?act=check_email', 'email=' + email, check_email_callback_login_up, 'GET', 'TEXT', true, true );
}


function check_email_callback_login_up(str)
{
	var submit_disabled = false;
  //alert(str);
  if(str==0)
  {
	 //alert(0);
	 document.getElementById('email_notice').innerHTML = "亲！此邮箱还没有注册哦";
	 document.forms['formUser'].elements['Submit'].disabled = 'disabled';
	 //document.forms['formUser'].elements['Submit'].class = 'zhuce';
	 document.forms['formUser'].elements['Submit'].value='不可登录'
	 
	  }
  if(str==1)
  {
	  //alert(1);
	  document.getElementById('email_notice').innerHTML = "亲！欢迎您回来";
	  document.forms['formUser'].elements['Submit'].disabled = '';
	  //document.forms['formUser'].elements['Submit'].class = 'zhuceab';
	  document.forms['formUser'].elements['Submit'].value='登录'
	  }
  
}





function check_email_callback(str)
{
	var submit_disabled = false;
  //alert(str);
  if(str==0)
  {
	 //alert(0);
	 document.getElementById('email_notice').innerHTML = "可以注册";
	 document.forms['formUser'].elements['Submit'].disabled = '';
	 //document.forms['formUser'].elements['Submit'].class = 'zhuce';
	 document.forms['formUser'].elements['Submit'].value='注册'
	 
	  }
  if(str==1)
  {
	  //alert(1);
	  document.getElementById('email_notice').innerHTML = "邮箱已经注册";
	  document.forms['formUser'].elements['Submit'].disabled = 'disabled';
	  //document.forms['formUser'].elements['Submit'].class = 'zhuceab';
	  document.forms['formUser'].elements['Submit'].value='此邮箱不能注册'
	  }
  
}

/* *
 * 处理注册用户
 */
function register()
{
  var frm  = document.forms['formUser'];
  //var username  = Utils.trim(frm.elements['username'].value);
  var username  = Utils.trim(frm.elements['usernamefirst'].value)+Utils.trim(frm.elements['usernamelast'].value);
  var email  = document.getElementById('email').value;;
  var leibie  = frm.elements['leibie'].value; 
  var password  = Utils.trim(frm.elements['password'].value);
  var confirm_password = Utils.trim(frm.elements['confirm_password'].value);
  var checked_agreement = frm.elements['agreement'].checked;
  var msn = frm.elements['extend_field1'] ? Utils.trim(frm.elements['extend_field1'].value) : '';
  var qq = frm.elements['extend_field2'] ? Utils.trim(frm.elements['extend_field2'].value) : '';
  var home_phone = frm.elements['extend_field4'] ? Utils.trim(frm.elements['extend_field4'].value) : '';
  var office_phone = frm.elements['extend_field3'] ? Utils.trim(frm.elements['extend_field3'].value) : '';
  var mobile_phone = frm.elements['extend_field5'] ? Utils.trim(frm.elements['extend_field5'].value) : '';
  var passwd_answer = frm.elements['passwd_answer'] ? Utils.trim(frm.elements['passwd_answer'].value) : '';
  var sel_question =  frm.elements['sel_question'] ? Utils.trim(frm.elements['sel_question'].value) : '';


  var msg = "";
  // 检查输入
  var msg = '';
 /* if (username.length == 0)
  {
    msg += username_empty + '\n';
  }
  else if (username.match(/^\s*$|^c:\\con\\con$|[%,\'\*\"\s\t\<\>\&\\]/))
  {
    msg += username_invalid + '\n';
  }
  else if (username.length < 3)
  {
    //msg += username_shorter + '\n';
  }
*/
  if (email.length == 0)
  {
    //msg += email_empty + '\n';
	msg += "email is empty" + '\n';
  }
  else
  {
    if ( ! (Utils.isEmail(email)))
    {
      msg += email_invalid + '\n';
    }
  }
  if (password.length == 0)
  {
    msg += password_empty + '\n';
  }
  else if (password.length < 6)
  {
    msg += password_shorter + '\n';
  }
  if (/ /.test(password) == true)
  {
	msg += passwd_balnk + '\n';
  }
  if (confirm_password != password )
  {
    msg += confirm_password_invalid + '\n';
  }
  if(checked_agreement != true)
  {
    msg += agreement + '\n';
  }

  if (msn.length > 0 && (!Utils.isEmail(msn)))
  {
    msg += msn_invalid + '\n';
  }

  if (qq.length > 0 && (!Utils.isNumber(qq)))
  {
    msg += qq_invalid + '\n';
  }

  if (office_phone.length>0)
  {
    var reg = /^[\d|\-|\s]+$/;
    if (!reg.test(office_phone))
    {
      msg += office_phone_invalid + '\n';
    }
  }
  if (home_phone.length>0)
  {
    var reg = /^[\d|\-|\s]+$/;

    if (!reg.test(home_phone))
    {
      msg += home_phone_invalid + '\n';
    }
  }
  if (mobile_phone.length>0)
  {
    var reg = /^[\d|\-|\s]+$/;
    if (!reg.test(mobile_phone))
    {
      msg += mobile_phone_invalid + '\n';
    }
  }
  if (passwd_answer.length > 0 && sel_question == 0 || document.getElementById('passwd_quesetion') && passwd_answer.length == 0)
  {
    msg += no_select_question + '\n';
  }

  for (i = 4; i < frm.elements.length - 4; i++)	// 从第五项开始循环检查是否为必填项
  {
	needinput = document.getElementById(frm.elements[i].name + 'i') ? document.getElementById(frm.elements[i].name + 'i') : '';

	if (needinput != '' && frm.elements[i].value.length == 0)
	{
	  msg += '- ' + needinput.innerHTML + msg_blank + '\n';
	}
  }

  if (msg.length > 0)
  {
    alert(msg);
    return false;
  }
  else
  {
    return true;
  }
}

/* *
 * 用户中心订单保存地址信息
 */
function saveOrderAddress(id)
{
  var frm           = document.forms['formAddress'];
  var consignee     = frm.elements['consignee'].value;
  var email         = frm.elements['email'].value;
  var address       = frm.elements['address'].value;
  var zipcode       = frm.elements['zipcode'].value;
  var tel           = frm.elements['tel'].value;
  var mobile        = frm.elements['mobile'].value;
  var sign_building = frm.elements['sign_building'].value;
  var best_time     = frm.elements['best_time'].value;

  if (id == 0)
  {
    alert(current_ss_not_unshipped);
    return false;
  }
  var msg = '';
  if (address.length == 0)
  {
    msg += address_name_not_null + "\n";
  }
  if (consignee.length == 0)
  {
    msg += consignee_not_null + "\n";
  }

  if (msg.length > 0)
  {
    alert(msg);
    return false;
  }
  else
  {
    return true;
  }
}

/* *
 * 会员余额申请
 */
function submitSurplus()
{
  var frm            = document.forms['formSurplus'];
  var surplus_type   = frm.elements['surplus_type'].value;
  var surplus_amount = frm.elements['amount'].value;
  var process_notic  = frm.elements['user_note'].value;
  var payment_id     = 0;
  var msg = '';
  //alert("充值");

  if (surplus_amount.length == 0 )
  {
    msg += "请输入您要操作的金额数量！" + "\n";
  }
  else
  {
    var reg = /^[\.0-9]+/;
    if ( ! reg.test(surplus_amount))
    {
      msg += "您输入的金额数量格式不正确！" + '\n';
    }
  }

  if (process_notic.length == 0)
  {
    msg += "请输入您此次操作的备注信息！" + "\n";
  }

  if (msg.length > 0)
  {
    alert(msg);
    return false;
  }

  if (surplus_type == 0)
  {
    for (i = 0; i < frm.elements.length ; i ++)
    {
      if (frm.elements[i].name=="payment_id" && frm.elements[i].checked)
      {
        payment_id = frm.elements[i].value;
        break;
      }
    }

    if (payment_id == 0)
    {
      alert("请选择支付方式");
      return false;
    }
  }

  return true;
}

/* *
 *  处理用户添加一个红包
 */
function addBonus()
{
  var frm      = document.forms['addBouns'];
  var bonus_sn = frm.elements['bonus_sn'].value;

  if (bonus_sn.length == 0)
  {
    alert(bonus_sn_empty);
    return false;
  }
  else
  {
    var reg = /^[0-9]{10}$/;
    if ( ! reg.test(bonus_sn))
    {
      alert(bonus_sn_error);
      return false;
    }
  }

  return true;
}

/* *
 *  合并订单检查
 */
function mergeOrder()
{
  if (!confirm(confirm_merge))
  {
    return false;
  }

  var frm        = document.forms['formOrder'];
  var from_order = frm.elements['from_order'].value;
  var to_order   = frm.elements['to_order'].value;
  var msg = '';

  if (from_order == 0)
  {
    msg += from_order_empty + '\n';
  }
  if (to_order == 0)
  {
    msg += to_order_empty + '\n';
  }
  else if (to_order == from_order)
  {
    msg += order_same + '\n';
  }
  if (msg.length > 0)
  {
    alert(msg);
    return false;
  }
  else
  {
    return true;
  }
}

/* *
 * 订单中的商品返回购物车
 * @param       int     orderId     订单号
 */
function returnToCart(orderId)
{
  Ajax.call('user.php?act=return_to_cart', 'order_id=' + orderId, returnToCartResponse, 'POST', 'JSON');
}

function returnToCartResponse(result)
{
  alert(result.message);
}

/* *
 * 检测密码强度
 * @param       string     pwd     密码
 */
function checkIntensity(pwd)
{
  var Mcolor = "#FFF",Lcolor = "#FFF",Hcolor = "#FFF";
  var m=0;

  var Modes = 0;
  for (i=0; i<pwd.length; i++)
  {
    var charType = 0;
    var t = pwd.charCodeAt(i);
    if (t>=48 && t <=57)
    {
      charType = 1;
    }
    else if (t>=65 && t <=90)
    {
      charType = 2;
    }
    else if (t>=97 && t <=122)
      charType = 4;
    else
      charType = 4;
    Modes |= charType;
  }

  for (i=0;i<4;i++)
  {
    if (Modes & 1) m++;
      Modes>>>=1;
  }

  if (pwd.length<=4)
  {
    m = 1;
  }

  switch(m)
  {
    case 1 :
      Lcolor = "2px solid red";
      Mcolor = Hcolor = "2px solid #DADADA";
    break;
    case 2 :
      Mcolor = "2px solid #f90";
      Lcolor = Hcolor = "2px solid #DADADA";
    break;
    case 3 :
      Hcolor = "2px solid #3c0";
      Lcolor = Mcolor = "2px solid #DADADA";
    break;
    case 4 :
      Hcolor = "2px solid #3c0";
      Lcolor = Mcolor = "2px solid #DADADA";
    break;
    default :
      Hcolor = Mcolor = Lcolor = "";
    break;
  }
  if (document.getElementById("pwd_lower"))
  {
    document.getElementById("pwd_lower").style.borderBottom  = Lcolor;
    document.getElementById("pwd_middle").style.borderBottom = Mcolor;
    document.getElementById("pwd_high").style.borderBottom   = Hcolor;
  }


}

function changeType(obj)
{
  if (obj.getAttribute("min") && document.getElementById("ECS_AMOUNT"))
  {
    document.getElementById("ECS_AMOUNT").disabled = false;
    document.getElementById("ECS_AMOUNT").value = obj.getAttribute("min");
    if (document.getElementById("ECS_NOTICE") && obj.getAttribute("to") && obj.getAttribute('fee'))
    {
      var fee = parseInt(obj.getAttribute("fee"));
      var to = parseInt(obj.getAttribute("to"));
      if (fee < 0)
      {
        to = to + fee * 2;
      }
      document.getElementById("ECS_NOTICE").innerHTML = notice_result + to;
    }
  }
}

function calResult()
{
  var amount = document.getElementById("ECS_AMOUNT").value;
  var notice = document.getElementById("ECS_NOTICE");

  reg = /^\d+$/;
  if (!reg.test(amount))
  {
    notice.innerHTML = notice_not_int;
    return;
  }
  amount = parseInt(amount);
  var frm = document.forms['transform'];
  for(i=0; i < frm.elements['type'].length; i++)
  {
    if (frm.elements['type'][i].checked)
    {
      var min = parseInt(frm.elements['type'][i].getAttribute("min"));
      var to = parseInt(frm.elements['type'][i].getAttribute("to"));
      var fee = parseInt(frm.elements['type'][i].getAttribute("fee"));
      var result = 0;
      if (amount < min)
      {
        notice.innerHTML = notice_overflow + min;
        return;
      }

      if (fee > 0)
      {
        result = (amount - fee) * to / (min -fee);
      }
      else
      {
        //result = (amount + fee* min /(to+fee)) * (to + fee) / min ;
        result = amount * (to + fee) / min + fee;
      }

      notice.innerHTML = notice_result + parseInt(result + 0.5);
    }
  }
}
