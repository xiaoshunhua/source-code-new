<?php include("../common/siteset.php");?>
<?php include("../common/init.php");?>
<?php include("../common/pager.class.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $sitename;?></title>
<link href="../styles/users.css" rel="stylesheet" type="text/css" />
<script src="js/user.js" type="text/javascript"></script>
<script src="js/sign_up.js" type="text/javascript"></script>
</head>
<body>
<?php include("../header.php");?>
<div class="bannerbj">
<div class="peoplezc">
<form action="sign_up_do.php" method="post" name="formUser" id="formUser" onsubmit="return InputCheck(this)">
<div class="zctable">
<h2>注册</h2>
<div class="xixian"></div>
<h3>姓名</h3>
<div style="width:400px; float:left;">
<div class="zcname"><input name="usernamefirst" type="text" size="15" id="usernamefirst" onblur="is_registeredusernamefirst(this.value);"value="姓氏" onFocus="if (value =='姓氏'){value =''}"onblur="if (value ==''){value='姓氏'};" /><span id="usernamefirst_notice" style="color:#FF0000"> </span></div>
<div class="zcname" style=" margin-left:13px;">
<input name="usernamelast" type="text" size="15" id="usernamelast" onblur="is_registeredusernamelast(this.value);"value="名字" onFocus="if (value =='名字'){value =''}"onblur="if (value ==''){value='名字'};" /><span id="usernamelast_notice" style="color:#FF0000"> </span></div>
</div>
<!----------------------email start----------------->
<h3>邮箱</h3>
<div style="width:400px; float:left;">
<div class="zcemail">
<input name="email" type="text" size="25" id="email" onblur="checkEmail(this.value);" /><span id="email_notice" style="color:#FF0000"> </span>
</div>
</div>
<!----------------------email end----------------->
<!----------------------password start----------------->
<h3>密码</h3>
<div style="width:400px; float:left;">
<div class="zcepassword">
<input name="password" type="password" id="password" onblur="check_password(this.value);" onkeyup="checkIntensity(this.value)" /><span style="color:#FF0000" id="password_notice"> </span> 
</div>
</div>
<!----------------------password end----------------->
<!----------------------submitpassword start----------------->
<h3>再次确认密码</h3>
<div style="width:400px; float:left;">
<div class="zcepassword">
<input name="conform_password" type="password" id="conform_password" onblur="check_conform_password(this.value);" onkeyup="checkIntensity(this.value)" /><span style="color:#FF0000" id="conform_password_notice"> </span>
</div>
</div>
<!----------------------submitpassword end----------------->
<div style="width:400px; float:left;">
<div  class="zcbt">
<input type="Submit" id="Submit" value="注册" disabled/>
</div></div>
<div style="width:400px; float:left;">
<div class="zcg">
<a href="<?php echo $localhost.$siteurl."users/login_up.php"?>" >已有账号? 这里登陆</a>
</div>
</div>
</div>
</form>
</div>
</div>
<?php include("../footer.php");?>



</body>
</html>