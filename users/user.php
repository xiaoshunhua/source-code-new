<?php include("../common/siteset.php");?>
<?php include("../common/init.php");?>
<?php include("../common/pager.class.php");?>
<?php session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $sitename;?></title>
<link href="../styles/users.css" rel="stylesheet" type="text/css" />
<link href="../styles/main.css" rel="stylesheet" type="text/css" />
<script src="js/user.js" type="text/javascript"></script>
<script src="../js/time.js" type="text/javascript"></script>
<style type="text/css">
.tableuserinformation{ border-collapse:collapse;}
.tableuserinformation td{ border:1px solid #dddddd;}
</style>
</head>
<body>
<?php
if(!isset($_SESSION["adminname"]) && $_SESSION["adminname"]=="")
{?>
<script type="text/javascript">alert("请先登录！");window.location = "<?php echo $localhost.$siteurl;?>users/login_up.php";</script>
<?php }
else
{
?>
<?php include("../header.php");?>
<div style="background-image:url(../images/membercenterbg.jpg); width:1024px; background-color:#fafafa;">
<div class="blank"></div>
<div class="block clearfix">
  <!--left start-->
  <div class="AreaL" style="width:320px;">
  <div style="width:320px;">
  <div style="margin-left:40px; " align="left";>
  <!------------会员头像及其信息start------------------->
<!----------------语句start----------------->
<?php  
$action=$_REQUEST['act'];
//echo $action;

 $cf=new CommonFunction();
 $cf->connect();
 $username=$_SESSION["admin"];
 $query = "select * from fp_users where user_email = '$username'";
 $userInfo = $cf->getRow_extend($query); 
if(empty($userInfo)) 
{ 
	echo"暂无此数据！";	
}

//echo $userInfo['user_register_time'];

?>
<!----------------语句end--------------->

<table  border="0"  cellpadding="5"><tr><td >
<?php if($userInfo['user_icon']!="") {?><img src="<?php echo $userInfo['user_icon'];?>" border="0" width="122px" /><?php }else{?><img src="../images/tx.jpg" border="0"  ><?php }?></td><td align="left">
<font size="+1"><?php echo $_SESSION['adminname']; ?></font><br />
<img src="../images/money.png"  border="0" style=" float:left; "/>
<div style="margin-top:5px; float:right; font-size:14px;">&nbsp;<?php echo $userInfo['user_money']; ?> 元</div>
</td>
</tr></table> 
<!------------会员头像及其信息end------------------->
  </div>
  </div>
    <div class="box" style="margin-top:20px;background-color:#fafafa">
     <div class="box_1"  >
      <div class="userCenterBox" style="border:1px solid #c7c7c7; background-color:#fafafa;-webkit-border-radius: 3.5px;
    -moz-border-radius: 3.5px; width:260px;"> 
    
      <div style="background-color:#be3120; width:260px; height:40px;border:1px solid #be3120;-webkit-border-top-left-radius: 3.5px;-webkit-border-top-right-radius: 3.5px;
    -moz-border-radius-topleft: 3.5px;-moz-border-radius-topright: 3.5px; margin-left:-1px; margin-right:-1px; margin-top:-1px; margin-bottom:10px; "></div>
        <div class="userMenu"  >
<a href="user.php?act=default"<?php if($action=='default'){?>class="curs"<?php }?>><img src="../images/u1.png"> 欢迎页</a>
<a href="user.php?act=profile"<?php if($action=='profile'){?>class="curs"<?php }?>><img src="../images/u2.png"> 用户信息</a>
<a href="user.php?act=order_list"<?php if($action=='order_list'){?>class="curs"<?php }?>><img src="../images/u3.png"> 我的订单</a>


<a href="user.php?act=account_log"<?php if($action=='account_log'){?>class="curs"<?php }?>><img src="../images/u13.png"> 资金管理</a>
</div>
      </div>
     </div>
    </div>
  </div>
  <!--left end-->
  <!--right start-->
  <div class="AreaR" style=" width:684px;">
    <div class="box">
     <div class="box_1">
      <div class="userCenterBox boxCenterList clearfix" style="_height:1%;">
      <!----------欢迎页start----------->
      <?php
      if($action=='default')
	  {
	 include("userdefault.php");
          }
	  ?>
      
      <!----------欢迎页end------------>
      
      
      <!-- 用户信息界面 start-->
      <?php
      if($action=='profile')
	  {
	 include("userprofile.php");
	  }
	  ?>
     <!--#用户信息界面 end-->
     <!--#订单列表界面 start-->
     <?php 
	  if($action=='order_list')
	  {
      include("user_order.php");
      }?>
       <!--#订单列表界面 end-->
       <?php if($action=='order_detail'){?>
     <!-- ==========订单详情页面,包括：订单状态，商品列表，费用总计，收货人信息，支付方式，其它信息========== -->
      <!--{if $action eq order_detail} -->
        <h5><span>{$lang.order_status}</span></h5>
        <div class="blank"></div>
         <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#dddddd">
        <tr>
          <td width="15%" align="right" bgcolor="#ffffff">{$lang.detail_order_sn}：</td>
         <td align="left" bgcolor="#ffffff">{$order.order_sn}
          <!-- {if $order.extension_code eq "group_buy"} -->
					<a href="./group_buy.php?act=view&id={$order.extension_id}" class="f6"><strong>{$lang.order_is_group_buy}</strong></a>
					<!-- {elseif $order.extension_code eq "exchange_goods"} -->
					<a href="./exchange.php?act=view&id={$order.extension_id}" class="f6"><strong>{$lang.order_is_exchange}</strong></a>
					<!--{/if}-->  
					<a href="user.php?act=message_list&order_id={$order.order_id}" class="f6">[{$lang.business_message}]</a>
					</td>
        </tr>
        <tr>
          <td align="right" bgcolor="#ffffff">{$lang.detail_order_status}：</td>
          <td align="left" bgcolor="#ffffff">{$order.order_status}&nbsp;&nbsp;&nbsp;&nbsp;{$order.confirm_time}</td>
        </tr>
        <tr>
          <td align="right" bgcolor="#ffffff">{$lang.detail_pay_status}：</td>
          <td align="left" bgcolor="#ffffff">{$order.pay_status}&nbsp;&nbsp;&nbsp;&nbsp;{if $order.order_amount gt 0}{$order.pay_online}{/if}{$order.pay_time}</td>
        </tr>
        <!------------配送状态start-------------------->
        <!--<tr>
          <td align="right" bgcolor="#ffffff">{$lang.detail_shipping_status}8：</td>
          <td align="left" bgcolor="#ffffff">{$order.shipping_status}&nbsp;&nbsp;&nbsp;&nbsp;{$order.shipping_time}</td>
        </tr>-->
        <!------------配送状态end-------------------->
        <!-- {if $order.invoice_no}-->
        <tr>
          <td align="right" bgcolor="#ffffff">{$lang.consignment}：</td>
          <td align="left" bgcolor="#ffffff">{$order.invoice_no}</td>
        </tr>
        <!--{/if}-->
        <!--{if $order.to_buyer}-->
        <tr>
          <td align="right" bgcolor="#ffffff">{$lang.detail_to_buyer}：</td>
          <td align="left" bgcolor="#ffffff">{$order.to_buyer}</td>
        </tr>
        <!-- {/if} -->

        <!--{if $virtual_card}-->
        <tr>
          <td align="right" bgcolor="#ffffff">{$lang.virtual_card_info}：</td>
          <td colspan="3" align="left" bgcolor="#ffffff">
          <!--{foreach from=$virtual_card item=vgoods}-->
            <!--{foreach from=$vgoods.info item=card}-->
              <!--{if $card.card_sn}-->{$lang.card_sn}:<span style="color:red;">{$card.card_sn}</span><!--{/if}-->
              <!--{if $card.card_password}-->{$lang.card_password}:<span style="color:red;">{$card.card_password}</span><!--{/if}-->
              <!--{if $card.end_date}-->{$lang.end_date}:{$card.end_date}<!--{/if}--><br />
            <!--{/foreach}-->
          <!--{/foreach}-->
          </td>
        </tr>
        <!--{/if}-->
      </table>
        <div class="blank"></div>
        <h5><span>{$lang.goods_list}</span>
        <!-- {if $allow_to_cart} -->
        <!--{if $order.pay_status=="已付款"}--><!--{else}-->
        <a href="javascript:;" onClick="returnToCart({$order.order_id})" class="f6">{$lang.return_to_cart}</a>
        <!--{/if}-->
        <!-- {/if} -->
        </h5>
        <div class="blank"></div>
         <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#dddddd">
          <tr>
            <th width="23%" align="center" bgcolor="#ffffff">{$lang.goods_name}</th>
            <th width="29%" align="center" bgcolor="#ffffff">{$lang.goods_attr}</th>
            <!--<th>{$lang.market_price}</th>-->
            <th width="26%" align="center" bgcolor="#ffffff">{$lang.goods_price}<!-- {if $order.extension_code eq "group_buy"} -->{$lang.gb_deposit}<!-- {/if} --></th>
            <th width="9%" align="center" bgcolor="#ffffff">{$lang.number}</th>
            <th width="20%" align="center" bgcolor="#ffffff">{$lang.subtotal}</th>
            
          </tr>
          <!-- {foreach from=$goods_list item=goods} -->
          <tr align="center">
            <td bgcolor="#ffffff">
              <!-- {if $goods.goods_id gt 0 && $goods.extension_code neq 'package_buy'} 商品 -->
                <a href="goods.php?id={$goods.goods_id}" target="_blank" class="f6">{$goods.goods_name}</a>
                <!-- {if $goods.parent_id > 0} -->
                <span style="color:#FF0000">（{$lang.accessories}）</span>
                <!-- {elseif $goods.is_gift} -->
                <span style="color:#FF0000">（{$lang.largess}）</span>
                <!-- {/if} -->
              <!-- {elseif $goods.goods_id gt 0 && $goods.extension_code eq 'package_buy'} -->
                <a href="javascript:void(0)" onClick="setSuitShow({$goods.goods_id})" class="f6">{$goods.goods_name}<span style="color:#FF0000;">（礼包）</span></a>
                <div id="suit_{$goods.goods_id}" style="display:none">
                    <!-- {foreach from=$goods.package_goods_list item=package_goods_list} -->
                      <a href="goods.php?id={$package_goods_list.goods_id}" target="_blank" class="f6">{$package_goods_list.goods_name}</a><br />
                    <!-- {/foreach} -->
                </div>
              <!-- {/if} -->
              </td>
            <td align="center" bgcolor="#ffffff">{$goods.goods_attr|nl2br}</td>
            <!--<td align="right">{$goods.market_price}</td>--> 
            <td align="center" bgcolor="#ffffff">{$goods.goods_price}</td> 
            <td align="center" bgcolor="#ffffff">{$goods.goods_number}</td>
            <td align="center" bgcolor="#ffffff">{$goods.subtotal}</td>
            
          </tr>
          <tr align="center">
          <td bgcolor="#ffffff"><strong>下载地址</strong></td><td  bgcolor="#ffffff"  colspan="8">
          <!--{if $order.pay_status=="已付款"}-->{if $file_url}<a href="{$file_url}"><img src="images/download.png"  style="width:30px; border:0px;"/></a>{else}暂未上传{/if}<!--{else}--><font color="#FF0000">未付款不能下载</font><!--{/if}-->
          </td>
          </tr>
          <tr align="center">
          <td bgcolor="#ffffff"><strong>注册密钥</strong></td><td colspan="8" bgcolor="#ffffff" align="center"><!--{if $order.pay_status=="已付款"}--><font size="-3" color="#FF0000">{$zhucemamiwen}</font><!--{else}--><font color="#FF0000">未付款不能查看</font><!--{/if}--></td></tr>
          
          <!-- {/foreach} -->
          <tr>
            <td colspan="8" bgcolor="#ffffff" align="right">
            {$lang.shopping_money}<!-- {if $order.extension_code eq "group_buy"} -->{$lang.gb_deposit}<!-- {/if} -->: {$order.formated_goods_amount}
            </td>
          </tr>
         
        </table>
         <div class="blank"></div>
        <h5><span>{$lang.fee_total}</span></h5>
        <div class="blank"></div>
         <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#dddddd">
          <tr>
            <td align="right" bgcolor="#ffffff">
                {$lang.goods_all_price}<!-- {if $order.extension_code eq "group_buy"} -->{$lang.gb_deposit}<!-- {/if} -->: {$order.formated_goods_amount}
              <!-- {if $order.discount gt 0} 折扣 -->
              - {$lang.discount}: {$order.formated_discount}
              <!-- {/if} -->
              <!-- {if $order.tax gt 0} -->
              + {$lang.tax}: {$order.formated_tax}
              <!-- {/if} -->
              <!-- {if $order.shipping_fee > 0} -->
              + {$lang.shipping_fee}: {$order.formated_shipping_fee}
              <!-- {/if} -->
              <!-- {if $order.insure_fee > 0} -->
              + {$lang.insure_fee}: {$order.formated_insure_fee}
              <!-- {/if} -->
              <!-- {if $order.pay_fee > 0} -->
              + {$lang.pay_fee}: {$order.formated_pay_fee}
              <!-- {/if} -->
              <!-- {if $order.pack_fee > 0} -->
              + {$lang.pack_fee}: {$order.formated_pack_fee}
              <!-- {/if} -->
              <!-- {if $order.card_fee > 0} -->
              + {$lang.card_fee}: {$order.formated_card_fee}
              <!-- {/if} -->        </td>
          </tr>
          <tr>
            <td align="right" bgcolor="#ffffff">
              <!-- {if $order.money_paid > 0} -->
               {$lang.order_money_paid}: {$order.formated_money_paid}
              <!-- {/if} -->
              <!-- {if $order.surplus > 0} -->
              - {$lang.use_surplus}: {$order.formated_surplus}
              <!-- {/if} -->
              <!-- {if $order.integral_money > 0} -->
              - {$lang.use_integral}: {$order.formated_integral_money}
              <!-- {/if} -->
              <!-- {if $order.bonus > 0} -->
              - {$lang.use_bonus}: {$order.formated_bonus}
              <!-- {/if} -->        </td>
          </tr>
          <tr>
            <td align="right" bgcolor="#ffffff">{$lang.order_amount}: {$order.formated_order_amount}
            <!-- {if $order.extension_code eq "group_buy"} --><br />
            {$lang.notice_gb_order_amount}<!-- {/if} --></td>
          </tr>
            <!-- {if $allow_edit_surplus} 如果可以编辑使用余额数 -->
          <tr>
            <td align="right" bgcolor="#ffffff">
      <form action="user.php" method="post" name="formFee" id="formFee">{$lang.use_more_surplus}:
            <input name="surplus" type="text" size="8" value="0" style="border:1px solid #ccc;"/>{$max_surplus}
            <input type="submit" name="Submit" class="submit" value="{$lang.button_submit}" />
      <input type="hidden" name="act" value="act_edit_surplus" />
      <input type="hidden" name="order_id" value="{$smarty.get.order_id}" />
      </form></td>
          </tr>
    <!--{/if}-->
        </table>
         <div class="blank"></div>
        <h5><span>{$lang.consignee_info}</span></h5>
        <div class="blank"></div>
         <!-- {if $order.allow_update_address gt 0} -->
          <form action="user.php" method="post" name="formAddress" id="formAddress">
           <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#dddddd">
              <tr>
                <td width="15%" align="right" bgcolor="#ffffff">{$lang.consignee_name}： </td>
                <td width="35%" align="left" bgcolor="#ffffff"><input name="consignee" type="text"  class="inputBg" value="{$order.consignee|escape}" size="25">
                </td>
                <td width="15%" align="right" bgcolor="#ffffff">{$lang.email_address}：</td>
                <td width="35%" align="left" bgcolor="#ffffff"><input name="email" type="text"  class="inputBg" value="{$order.email|escape}" size="25" />
                </td>
              </tr>
              <!-- {if $order.exist_real_goods} -->
              <!-- 只有虚拟商品处理-->
              <tr>
                <td align="right"bgcolor="#ffffff">{$lang.detailed_address}： </td>
                <td align="left" bgcolor="#ffffff"><input name="address" type="text" class="inputBg" value="{$order.address|escape} " size="25" /></td>
                <td align="right" bgcolor="#ffffff">{$lang.postalcode}：</td>
                <td align="left" bgcolor="#ffffff"><input name="zipcode" type="text"  class="inputBg" value="{$order.zipcode|escape}" size="25" /></td>
              </tr>
              <!--{/if}-->
              <tr>
                <td align="right" bgcolor="#ffffff">{$lang.phone}：</td>
                <td align="left" bgcolor="#ffffff"><input name="tel" type="text" class="inputBg" value="{$order.tel|escape}" size="25" /></td>
                <td align="right" bgcolor="#ffffff">{$lang.backup_phone}：</td>
                <td align="left" bgcolor="#ffffff"><input name="mobile" type="text"  class="inputBg" value="{$order.mobile|escape}" size="25" /></td>
              </tr>
              <!-- {if $order.exist_real_goods} -->
              <!-- 只有虚拟商品处理-->
              <tr>
                <td align="right" bgcolor="#ffffff">{$lang.sign_building}：</td>
                <td align="left" bgcolor="#ffffff"><input name="sign_building" class="inputBg" type="text" value="{$order.sign_building|escape}" size="25" />
                </td>
                <td align="right" bgcolor="#ffffff">{$lang.deliver_goods_time}：</td>
                <td align="left" bgcolor="#ffffff"><input name="best_time" type="text" class="inputBg" value="{$order.best_time|escape}" size="25" />
                </td>
              </tr>
              <!-- {/if}-->
              <tr>
                <td colspan="4" align="center" bgcolor="#ffffff"><input type="hidden" name="act" value="save_order_address" />
                  <input type="hidden" name="order_id" value="{$order.order_id}" />
                  <input type="submit" class="bnt_blue_2" value="{$lang.update_address}"  />
                </td>
              </tr>
            </table>
          </form>
          <!-- {else} -->
          <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#dddddd">
            <tr>
              <td width="15%" align="right" bgcolor="#ffffff">{$lang.consignee_name}：</td>
              <td width="35%" align="left" bgcolor="#ffffff">{$order.consignee}</td>
              <td width="15%" align="right" bgcolor="#ffffff" >{$lang.email_address}：</td>
              <td width="35%" align="left" bgcolor="#ffffff">{$order.email}</td>
            </tr>
            <!-- {if $order.exist_real_goods} -->
            <tr>
              <td align="right" bgcolor="#ffffff">{$lang.detailed_address}：</td>
              <td colspan="3" align="left" bgcolor="#ffffff">{$order.address}
                <!-- {if $order.zipcode} -->
                [{$lang.postalcode}: {$order.zipcode}]
                <!-- {/if} --></td>
            </tr>
            <!-- {/if}-->
            <tr>
              <td align="right" bgcolor="#ffffff">{$lang.phone}：</td>
              <td align="left" bgcolor="#ffffff">{$order.tel} </td>
              <td align="right" bgcolor="#ffffff">{$lang.backup_phone}：</td>
              <td align="left" bgcolor="#ffffff">{$order.mobile}</td>
            </tr>
            <!-- {if $order.exist_real_goods} -->
            <tr>
              <td align="right" bgcolor="#ffffff" >{$lang.sign_building}：</td>
              <td align="left" bgcolor="#ffffff">{$order.sign_building} </td>
              <td align="right" bgcolor="#ffffff" >{$lang.deliver_goods_time}：</td>
              <td align="left" bgcolor="#ffffff">{$order.best_time}</td>
            </tr>
            <!--{/if}-->
          </table>
          <!-- {/if} -->
          <div class="blank"></div>
        <h5><span>{$lang.payment}</span></h5>
        <div class="blank"></div>
        <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#dddddd">
                <tr>
                  <td bgcolor="#ffffff">
                  {$lang.select_payment}: {$order.pay_name}。{$lang.order_amount}: <strong>{$order.formated_order_amount}</strong><br />
                  {$order.pay_desc}
                  </td>
                </tr>
                  <tr>
                  <td bgcolor="#ffffff" align="right">
                  <!--{if $payment_list}-->
              <form name="payment" method="post" action="user.php">
              {$lang.change_payment}:
              <select name="pay_id">
                <!--{foreach from=$payment_list item=payment}-->
                <option value="{$payment.pay_id}">
                {$payment.pay_name}({$lang.pay_fee}:{$payment.format_pay_fee})
                </option>
                <!--{/foreach}-->
              </select>
              <input type="hidden" name="act" value="act_edit_payment" />
              <input type="hidden" name="order_id" value="{$order.order_id}" />
              <input type="submit" name="Submit" class="submit" value="{$lang.button_submit}" />
              </form>
              <!--{/if}-->
                  </td>
                </tr>
              </table>
        <div class="blank"></div>
        <h5><span>{$lang.other_info}</span></h5>
        <div class="blank"></div>
        <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#dddddd">
          <!-- {if $order.shipping_id > 0} -->
          <tr>
            <td width="15%" align="right" bgcolor="#ffffff">{$lang.shipping}：</td>
            <td colspan="3" width="85%" align="left" bgcolor="#ffffff">{$order.shipping_name}</td>
          </tr>
          <!-- {/if} -->

          <tr>
            <td width="15%" align="right" bgcolor="#ffffff">{$lang.payment}：</td>
            <td colspan="3" align="left" bgcolor="#ffffff">{$order.pay_name}</td>
          </tr>
          <!--{if $order.insure_fee > 0}-->
          <!--{/if}-->
          <!-- {if $order.pack_name} 是否使用包装 -->
          <tr>
            <td width="15%" align="right" bgcolor="#ffffff">{$lang.use_pack}：</td>
            <td colspan="3" align="left" bgcolor="#ffffff">{$order.pack_name}</td>
          </tr>
          <!-- {/if} 是否使用包装 -->
          <!-- {if $order.card_name} 是否使用贺卡 -->
          <tr>
            <td width="15%" align="right" bgcolor="#ffffff">{$lang.use_card}：</td>
            <td colspan="3" align="left" bgcolor="#ffffff">{$order.card_name}</td>
          </tr>
          <!-- {/if} -->
          <!-- {if $order.card_message} 是否使用贺卡 -->
          <tr>
            <td width="15%" align="right" bgcolor="#ffffff">{$lang.bless_note}：</td>
            <td colspan="3" align="left" bgcolor="#ffffff">{$order.card_message}</td>
          </tr>
          <!-- {/if} 是否使用贺卡 -->
          <!-- {if $order.surplus > 0} 是否使用余额 -->
          <!-- {/if} -->
          <!-- {if $order.integral > 0} 是否使用积分 -->
          <tr>
            <td width="15%" align="right" bgcolor="#ffffff">{$lang.use_integral}：</td>
            <td colspan="3" align="left" bgcolor="#ffffff">{$order.integral}</td>
          </tr>
          <!-- {/if} 是否使用积分 -->
          <!-- {if $order.bonus > 0} 是否使用红包 -->
          <!-- {/if} -->
          <!-- {if $order.inv_payee && $order.inv_content} 是否开发票 -->
          <tr>
            <td width="15%" align="right" bgcolor="#ffffff">{$lang.invoice_title}：</td>
            <td width="36%" align="left" bgcolor="#ffffff">{$order.inv_payee}</td>
            <td width="19%" align="right" bgcolor="#ffffff">{$lang.invoice_content}：</td>
            <td width="25%" align="left" bgcolor="#ffffff">{$order.inv_content}</td>
          </tr>
          <!-- {/if} -->
          <!-- {if $order.postscript} 是否有订单附言 -->
          <tr>
            <td width="15%" align="right" bgcolor="#ffffff">{$lang.order_postscript}：</td>
            <td colspan="3" align="left" bgcolor="#ffffff">{$order.postscript}</td>
          </tr>
          <!-- {/if} -->
          <tr>
            <td width="15%" align="right" bgcolor="#ffffff">{$lang.booking_process}：</td>
            <td colspan="3" align="left" bgcolor="#ffffff">{$order.how_oos_name}</td>
          </tr>
        </table>
      <!--{/if} -->
    <!--#订单详情页 end-->
    <?php }?>
    <!--#会员余额 start-->
        <?php include("user_account.php");?>
    <!--#会员余额 end-->
      <!--#收货地址页面 -->
      <?php if($action=='address_list'){?>
      <!--{if $action eq 'address_list'} -->
        <h5><span>{$lang.consignee_info}</span></h5>
        <div class="blank"></div>
         {* 包含脚本文件 *}
            {insert_scripts files='utils.js,transport.js,region.js,shopping_flow.js'}
            
            <!-- {foreach from=$consignee_list item=consignee key=sn} -->
            <form action="user.php" method="post" name="theForm" onSubmit="return checkConsignee(this)">
              <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#dddddd">
                <tr>
                  <td align="right" bgcolor="#ffffff">{$lang.country_province}：</td>
                  <td colspan="3" align="left" bgcolor="#ffffff"><select name="country" id="selCountries_{$sn}" onChange="region.changed(this, 1, 'selProvinces_{$sn}')">
                      <option value="0">{$lang.please_select}{$name_of_region[0]}</option>
                      <!-- {foreach from=$country_list item=country} -->
                      <option value="{$country.region_id}" {if $consignee.country eq $country.region_id}selected{/if}>{$country.region_name}</option>
                      <!-- {/foreach} -->
                    </select>
                    <select name="province" id="selProvinces_{$sn}" onChange="region.changed(this, 2, 'selCities_{$sn}')">
                      <option value="0">{$lang.please_select}{$name_of_region[1]}</option>
                      <!-- {foreach from=$province_list.$sn item=province} -->
                      <option value="{$province.region_id}" {if $consignee.province eq $province.region_id}selected{/if}>{$province.region_name}</option>
                      <!-- {/foreach} -->
                    </select>
                    <select name="city" id="selCities_{$sn}" onChange="region.changed(this, 3, 'selDistricts_{$sn}')">
                      <option value="0">{$lang.please_select}{$name_of_region[2]}</option>
                      <!-- {foreach from=$city_list.$sn item=city} -->
                      <option value="{$city.region_id}" {if $consignee.city eq $city.region_id}selected{/if}>{$city.region_name}</option>
                      <!-- {/foreach} -->
                    </select>
                    <select name="district" id="selDistricts_{$sn}" {if !$district_list.$sn}style="display:none"{/if}>
                      <option value="0">{$lang.please_select}{$name_of_region[3]}</option>
                      <!-- {foreach from=$district_list.$sn item=district} -->
                      <option value="{$district.region_id}" {if $consignee.district eq $district.region_id}selected{/if}>{$district.region_name}</option>
                      <!-- {/foreach} -->
                    </select>
                  {$lang.require_field} </td>
                </tr>
                <tr>
                  <td align="right" bgcolor="#ffffff">{$lang.consignee_name}：</td>
                  <td align="left" bgcolor="#ffffff"><input name="consignee" type="text" class="inputBg" id="consignee_{$sn}" value="{$consignee.consignee|escape}" />
                  {$lang.require_field} </td>
                  <td align="right" bgcolor="#ffffff">{$lang.email_address}：</td>
                  <td align="left" bgcolor="#ffffff"><input name="email" type="text" class="inputBg" id="email_{$sn}" value="{$consignee.email|escape}" />
                  {$lang.require_field}</td>
                </tr>
                <tr>
                  <td align="right" bgcolor="#ffffff">{$lang.detailed_address}：</td>
                  <td align="left" bgcolor="#ffffff"><input name="address" type="text" class="inputBg" id="address_{$sn}" value="{$consignee.address|escape}" />
                  {$lang.require_field}</td>
                  <td align="right" bgcolor="#ffffff">{$lang.postalcode}：</td>
                  <td align="left" bgcolor="#ffffff"><input name="zipcode" type="text" class="inputBg" id="zipcode_{$sn}" value="{$consignee.zipcode|escape}" /></td>
                </tr>
                <tr>
                  <td align="right" bgcolor="#ffffff">{$lang.phone}：</td>
                  <td align="left" bgcolor="#ffffff"><input name="tel" type="text" class="inputBg" id="tel_{$sn}" value="{$consignee.tel|escape}" />
                  {$lang.require_field}</td>
                  <td align="right" bgcolor="#ffffff">{$lang.backup_phone}：</td>
                  <td align="left" bgcolor="#ffffff"><input name="mobile" type="text" class="inputBg" id="mobile_{$sn}" value="{$consignee.mobile|escape}" /></td>
                </tr>
                <tr>
                  <td align="right" bgcolor="#ffffff">{$lang.sign_building}：</td>
                  <td align="left" bgcolor="#ffffff"><input name="sign_building" type="text" class="inputBg" id="sign_building_{$sn}" value="{$consignee.sign_building|escape}" /></td>
                  <td align="right" bgcolor="#ffffff">{$lang.deliver_goods_time}：</td>
                  <td align="left" bgcolor="#ffffff"><input name="best_time" type="text"  class="inputBg" id="best_time_{$sn}" value="{$consignee.best_time|escape}" /></td>
                </tr>
                <tr>
                  <td align="right" bgcolor="#ffffff">&nbsp;</td>
                  <td colspan="3" align="center" bgcolor="#ffffff"><!-- {if $consignee.consignee && $consignee.email} -->
                    <input type="submit" name="submit" class="bnt_blue_1" value="{$lang.confirm_edit}" />
                    <input name="button" type="button" class="bnt_blue"  onclick="if (confirm('{$lang.confirm_drop_address}'))location.href='user.php?act=drop_consignee&id={$consignee.address_id}'" value="{$lang.drop}" />
                    <!-- {else} -->
                    <input type="submit" name="submit" class="bnt_blue_2"  value="{$lang.add_address}"/>
                    <!-- {/if} -->
                    <input type="hidden" name="act" value="act_edit_address" />
                    <input name="address_id" type="hidden" value="{$consignee.address_id}" />
                  </td>
                </tr>
              </table>
            </form>
            <!-- {/foreach} -->
      <!--{/if} -->
    <!--#收货地址添加页面 -->
    <?php }?>
    <?php if($action=='transform_points'){?>
      <!--* 积分兑换-->
       <!--{if $action eq 'transform_points'} -->
       <h5><span>{$lang.transform_points}</span></h5>
             <div class="blank"></div>
       <!--{if $exchange_type eq 'ucenter'}-->
        <form action="user.php" method="post" name="transForm" onSubmit="return calcredit();">
       <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#dddddd">
                <tr>
                    <th width="120" bgcolor="#FFFFFF" align="right" valign="top">{$lang.cur_points}:</th>
                    <td bgcolor="#FFFFFF">
                    <label for="pay_points">{$lang.exchange_points.1}:</label><input type="text" size="15" id="pay_points" name="pay_points" value="{$shop_points.pay_points}" style="border:0;border-bottom:1px solid #DADADA;" readonly="readonly" /><br />
                    <div class="blank"></div>
                    <label for="rank_points">{$lang.exchange_points.0}:</label><input type="text" size="15" id="rank_points" name="rank_points" value="{$shop_points.rank_points}" style="border:0;border-bottom:1px solid #DADADA;" readonly="readonly" />
                    </td>
                    </tr>
          <tr><td bgcolor="#FFFFFF">&nbsp;</td>
          <td bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <th align="right" bgcolor="#FFFFFF"><label for="amount">{$lang.exchange_amount}:</label></th>
            <td bgcolor="#FFFFFF"><input size="15" name="amount" id="amount" value="0" onKeyUp="calcredit();" type="text" />
                <select name="fromcredits" id="fromcredits" onChange="calcredit();">
                  {html_options options=$lang.exchange_points selected=$selected_org}
                </select>
            </td>
          </tr>
          <tr>
            <th align="right" bgcolor="#FFFFFF"><label for="desamount">{$lang.exchange_desamount}:</label></th>
            <td bgcolor="#FFFFFF"><input type="text" name="desamount" id="desamount" disabled="disabled" value="0" size="15" />
              <select name="tocredits" id="tocredits" onChange="calcredit();">
                {html_options options=$to_credits_options selected=$selected_dst}
              </select>
            </td>
          </tr>
          <tr>
            <th align="right" bgcolor="#FFFFFF">{$lang.exchange_ratio}:</th>
            <td bgcolor="#FFFFFF">1 <span id="orgcreditunit">{$orgcreditunit}</span> <span id="orgcredittitle">{$orgcredittitle}</span> {$lang.exchange_action} <span id="descreditamount">{$descreditamount}</span> <span id="descreditunit">{$descreditunit}</span> <span id="descredittitle">{$descredittitle}</span></td>
          </tr>
          <tr><td bgcolor="#FFFFFF">&nbsp;</td>
          <td bgcolor="#FFFFFF"><input type="hidden" name="act" value="act_transform_ucenter_points" /><input type="submit" name="transfrom" value="{$lang.transform}" /></td></tr>
  </table>
        </form>
       
       <!--{else}-->
        <b>{$lang.cur_points}:</b>
        <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#dddddd">
          <tr>
            <td width="30%" valign="top" bgcolor="#FFFFFF"><table border="0">
              <!--{foreach from=$bbs_points item=points}-->
              <tr>
                <th>{$points.title}:</th>
                <td width="120" style="border-bottom:1px solid #DADADA;">{$points.value}</td>
              </tr>
              <!--{/foreach} -->
            </table></td>
            <td width="50%" valign="top" bgcolor="#FFFFFF"><table>
                    <tr>
                <th>{$lang.pay_points}:</th>
                <td width="120" style="border-bottom:1px solid #DADADA;">{$shop_points.pay_points}</td>
                    </tr>
              <tr>
                <th>{$lang.rank_points}:</th>
                <td width="120" style="border-bottom:1px solid #DADADA;">{$shop_points.rank_points}</td>
              </tr>
            </table></td>
          </tr>
        </table>
        <br />
        <b>{$lang.rule_list}:</b>
        <ul class="point clearfix">
          <!-- {foreach from=$rule_list item=rule} -->
          <li><font class="f1">·</font>"{$rule.from}" {$lang.transform} "{$rule.to}" {$lang.rate_is} {$rule.rate}
          <!-- {/foreach} -->
        </ul>

        <form action="user.php" method="post" name="theForm">
        <table width="100%" border="1" align="center" cellpadding="5" cellspacing="0" style="border-collapse:collapse;border:1px solid #DADADA;">
          <tr style="background:#F1F1F1;">
            <th>{$lang.rule}</th>
            <th>{$lang.transform_num}</th>
            <th>{$lang.transform_result}</th>
          </tr>
          <tr>
            <td>
              <select name="rule_index" onChange="changeRule()">
                <!--{foreach from=$rule_list item=rule key=key}-->
                <option value="{$key}">{$rule.from}->{$rule.to}</option>
                <!--{/foreach}-->
              </select>
          </td>
          <td>
            <input type="text" name="num" value="0" onKeyUp="calPoints()"/>
          </td>
          <td><span id="ECS_RESULT">0</span></td>
          </tr>
          <tr>
            <td colspan="3" align="center"><input type="hidden" name="act" value="act_transform_points"  /><input type="submit" value="{$lang.transform}" /></td>
          </tr>
        </table>
        </form>
       
       <!--{/if}-->
        <!--{/if} -->
        <!--#积分兑换 -->
<?php }?>



      </div>
     </div>
    </div>
  </div>
  <!--right end-->
</div>
<div class="blank"></div>
</div>
<?php include("../footer.php");?>

<?php
/* 修改个人资料的处理 start*/
if ($action == 'act_edit_profile')
{
	$user_birthday = trim($_POST['user_birthday']);
    $user_email = trim($_POST['email']);
	$user_icon = trim($_POST['touxiang']);
	$user_gender = trim($_POST['user_gender']);
	$user_leibie = trim($_POST['leibie']);
	$user_msn = trim($_POST['user_msn']);
	$user_qq = trim($_POST['user_qq']);
	$user_office_phone = trim($_POST['user_office_phone']);
	$user_home_phone = trim($_POST['user_home_phone']);
	$user_mobile_phone = trim($_POST['user_mobile_phone']);
	$user_security_answer = trim($_POST['user_security_answer']);
	$user_security_question = trim($_POST['user_security_question']);
	//echo $user_birthday.$user_email.$user_icon.$user_gender.$user_leibie.$user_security_answer.$_SESSION["user_id"];	
	$table='fp_users';	
	$name="user_id";
    $value="'".$_SESSION["user_id"]."'";
	$query="user_icon="."'".$user_icon."'".","."user_birthday="."'".$user_birthday."'".","."user_gender="."'".$user_gender."'".","."user_email="."'".$user_email."'".","."user_leibie="."'".$user_leibie."'".","."user_security_answer="."'".$user_security_answer."'".","."user_security_question="."'".$user_security_question."'".","."user_msn="."'".$user_msn."'".","."user_qq="."'".$user_qq."'".","."user_office_phone="."'".$user_office_phone."'".","."user_home_phone="."'".$user_home_phone."'".","."user_mobile_phone="."'".$user_mobile_phone."'";
    $cf->user_edit($table,$query,$name,$value);	
	$cf->close();
	?>
	<script type="text/javascript">/*alert('test');*/window.location = "<?php echo $localhost.$siteurl;?>users/user.php?act=profile";</script>
	<?php	
}
/* 修改个人资料的处理 end*/

/* 修改个人密码的处理 start*/
if ($action == 'act_edit_password')
{
	$user_old_password = md5(trim($_POST['user_old_password']));
	$user_new_password = md5(trim($_POST['user_new_password']));
	//判断旧密码是否正确start
	$user_id=$_SESSION["user_id"];
	$query = "select user_name,user_last_login_time from fp_users where user_id = '$user_id' and user_password='$user_old_password'";
    $userInfo = $cf->getRow_extend($query);
if (empty($userInfo)) 
{
	?><script type="text/javascript">alert('旧密码错误');window.location = "<?php echo $localhost.$siteurl;?>users/user.php?act=profile";</script><?php
    
}
else
{
	//判断旧密码是否正确end
	
	$table='fp_users';	
	$name="user_id";
    $value="'".$_SESSION["user_id"]."'";
	$query="user_password="."'".$user_new_password."'".","."user_reset_password_date="."'".date("Y-m-d H:i:s")."'";
    $cf->user_edit($table,$query,$name,$value);	
	$cf->close();
	?>
	<script type="text/javascript">alert('密码修改成功');window.location = "<?php echo $localhost.$siteurl;?>users/user.php?act=profile";</script>
	<?php
}
/* 修改个人密码的处理 end*/
}



//充值金额start
if ($action == 'account_deposit_chongzhi')
{
  //取出支付方式start
  $payment_id=$_REQUEST['payment_id'];
  $query = "select * from fp_payments where payment_id = '$payment_id'";
  $userInfo = $cf->getRow_extend($query); 
if(empty($userInfo)) 
{ 
	echo"暂无此数据！";	
}
  $payment=$userInfo['payment_name'];
  //取出支付方式end
  $table="fp_users_account";//数据表
  $name="user_id".","."amount".","."add_time".","."user_note".","."payment_id".","."payment";
  $value="'".$_SESSION["user_id"]."'".","."'".$_REQUEST['amount']."'".","."'".date("Y-m-d H:i:s")."'".","."'".$_REQUEST['user_note']."'".","."'".$payment_id."'".","."'".$payment."'";
  $cf->fn_insert_sign_up_extend($table,$name,$value);
  //$cf->close();	
	
	?>
	<script type="text/javascript">alert('充值成功<?php echo $payment;?>');window.location = "<?php echo $localhost.$siteurl;?>users/user.php?act=account_log";</script>
	<?php
}
//充值金额end


//取消充值start

if ($action == 'cancel')
{
	$table='fp_users_account';	
	$name="id";
    $value="'".$_REQUEST['id']."'";
	$query="process_type=1";
    $cf->user_edit($table,$query,$name,$value);	
	$cf->close();
	?>
	<script type="text/javascript">alert('成功取消');window.location ="<?php echo $localhost.$siteurl;?>users/user.php?act=account_log";</script>
	<?php
}
//取消充值end

//取现start
if($action=='act_account_raply')
{
  //$amount=-$_REQUEST['amount'];取现为负数
  $table="fp_users_account";//数据表
  $payment_id=1;//余额取现
  $payment='余额支付';
  $name="user_id".","."amount".","."add_time".","."user_note".","."payment_id".","."payment";
  $value="'".$_SESSION["user_id"]."'".","."'".-$_REQUEST['amount']."'".","."'".date("Y-m-d H:i:s")."'".","."'".$_REQUEST['user_note']."'".","."'".$payment_id."'".","."'".$payment."'";
  $cf->fn_insert_sign_up_extend($table,$name,$value);
  //$cf->close();
  ?>
	<script type="text/javascript">alert('提交成功');window.location ="<?php echo $localhost.$siteurl;?>users/user.php?act=account_log";</script>
	<?php
}
//取现end

?>     






<?php
}
?>


</body>
</html>