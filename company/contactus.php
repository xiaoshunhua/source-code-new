<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="../favicon.ico" />
<link href="styles/main.css" rel="stylesheet" type="text/css" />
<link href="styles/pager.css" type="text/css" rel="stylesheet" />
<link href="styles/flickerplate.css" rel="stylesheet" type="text/css" />
<script src="js/min/jquery-v1.10.2.min.js" type="text/javascript"></script>
<script src="js/min/modernizr-custom-v2.7.1.min.js" type="text/javascript"></script>
<script src="js/min/jquery-finger-v0.1.0.min.js" type="text/javascript"></script>
<script src="js/min/flickerplate.min.js" type="text/javascript"></script>
<script>
	$(document).ready(function(){
		
		$('.flicker-example').flicker();
	});
	</script>
<?php include("../header.php");?>