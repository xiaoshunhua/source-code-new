-- phpMyAdmin SQL Dump
-- version 3.3.8
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2014 年 10 月 18 日 09:59
-- 服务器版本: 5.1.52
-- PHP 版本: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `fp_db_system`
--

-- --------------------------------------------------------

--
-- 表的结构 `classtest`
--

CREATE TABLE IF NOT EXISTS `classtest` (
  `user_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(60) NOT NULL DEFAULT '',
  `user_name` varchar(60) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `question` varchar(255) NOT NULL DEFAULT '',
  `answer` varchar(255) NOT NULL DEFAULT '',
  `sex` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `birthday` date NOT NULL DEFAULT '0000-00-00',
  `user_money` decimal(10,2) NOT NULL DEFAULT '0.00',
  `frozen_money` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pay_points` int(10) unsigned NOT NULL DEFAULT '0',
  `rank_points` int(10) unsigned NOT NULL DEFAULT '0',
  `address_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0',
  `last_login` int(11) unsigned NOT NULL DEFAULT '0',
  `last_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_ip` varchar(15) NOT NULL DEFAULT '',
  `visit_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `user_rank` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_special` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ec_salt` varchar(10) DEFAULT NULL,
  `salt` varchar(10) NOT NULL DEFAULT '0',
  `parent_id` mediumint(9) NOT NULL DEFAULT '0',
  `flag` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `alias` varchar(60) NOT NULL,
  `msn` varchar(60) NOT NULL,
  `qq` varchar(20) NOT NULL,
  `office_phone` varchar(20) NOT NULL,
  `home_phone` varchar(20) NOT NULL,
  `mobile_phone` varchar(20) NOT NULL,
  `is_validated` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `credit_line` decimal(10,2) unsigned NOT NULL,
  `passwd_question` varchar(50) DEFAULT NULL,
  `passwd_answer` varchar(255) DEFAULT NULL,
  `leibie` int(255) NOT NULL,
  `touxiang` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  KEY `email` (`email`),
  KEY `parent_id` (`parent_id`),
  KEY `flag` (`flag`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- 转存表中的数据 `classtest`
--

INSERT INTO `classtest` (`user_id`, `email`, `user_name`, `password`, `question`, `answer`, `sex`, `birthday`, `user_money`, `frozen_money`, `pay_points`, `rank_points`, `address_id`, `reg_time`, `last_login`, `last_time`, `last_ip`, `visit_count`, `user_rank`, `is_special`, `ec_salt`, `salt`, `parent_id`, `flag`, `alias`, `msn`, `qq`, `office_phone`, `home_phone`, `mobile_phone`, `is_validated`, `credit_line`, `passwd_question`, `passwd_answer`, `leibie`, `touxiang`) VALUES
(23, '000@126.com', '陈圆圆', '32c5ea7a238dac5eb6eeff3e2121cbe3', '', '', 0, '1954-01-01', '0.00', '0.00', 0, 0, 7, 1406792510, 1408929040, '0000-00-00 00:00:00', '127.0.0.1', 4, 0, 0, '8343', '0', 0, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 2, 'images/human/cloudy20081116225324981.png'),
(2, 'vip@ecshop.com', 'vip', '232059cb5361a9336ccf1b8c2ba7657a', '', '', 0, '1949-01-01', '0.00', '0.00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '', 0, 0, 0, NULL, '0', 0, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 0, ''),
(3, 'text@ecshop.com', 'text', '1cb251ec0d568de6a929b520c4aed8d1', '', '', 0, '1949-01-01', '1000.00', '0.00', 0, 0, 2, 0, 1242973574, '0000-00-00 00:00:00', '0.0.0.0', 2, 0, 0, NULL, '0', 0, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 0, ''),
(5, 'zuanshi@ecshop.com', 'zuanshi', '815a71fb334412e7ba4595741c5a111d', '', '', 0, '1949-01-01', '0.00', '10000.00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '', 0, 0, 0, NULL, '0', 0, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 0, ''),
(6, '2@126.com', 'test2', '104c5d07de4936e265c44ffe5e2c26fb', '', '', 2, '1986-04-01', '0.00', '0.00', 0, 0, 0, 1404639744, 1405298910, '0000-00-00 00:00:00', '127.0.0.1', 3, 0, 0, '7002', '0', 0, 0, '', '2@126.com', '3242423423', '65432456', '65432456', '15834532346', 0, '0.00', 'friend_birthday', '0401', 2, 'images/human/cloudylogo-5.png'),
(7, '3@126.com', 'test3', '1d01ed8739d50fd35466eadcffe4b05b', '', '', 0, '1954-01-01', '0.00', '0.00', 0, 0, 0, 1404640230, 1404975042, '0000-00-00 00:00:00', '127.0.0.1', 3, 0, 0, '8623', '0', 0, 0, '', '3@126.com', '3242423423', '54364532', '54364532', '15809853421', 0, '0.00', 'old_address', '深圳', 2, 'images/human/cloudylogo-6x5.png'),
(8, '5@126.com', 'test5', 'c20d17625e6e81eb4e63b286e96ce60f', '', '', 1, '1954-01-01', '0.00', '0.00', 0, 0, 0, 1404694006, 1404974050, '0000-00-00 00:00:00', '127.0.0.1', 14, 0, 0, '9356', '0', 0, 0, '', '5@126.com', '46465151', '54128745', '54128745', '158632547852', 0, '0.00', 'motto', '深圳', 2, ''),
(9, '6@126.com', 'test6', 'a12ec47b8609343421afe83c9146fcab', '', '', 0, '0000-00-00', '0.00', '0.00', 0, 0, 0, 1404786035, 1404809128, '0000-00-00 00:00:00', '127.0.0.1', 3, 0, 0, '5397', '0', 0, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 2, ''),
(10, '7@126.com', '7777', '1995716770db0828422fa852b8b68cc2', '', '', 0, '0000-00-00', '0.00', '0.00', 0, 0, 0, 1404787241, 1404787241, '0000-00-00 00:00:00', '127.0.0.1', 1, 0, 0, NULL, '0', 0, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 1, ''),
(11, '8@126.com', 'shun', '41d279ac304d82da14acfbff5208d479', '', '', 0, '1954-01-01', '0.00', '0.00', 0, 0, 0, 1404787426, 1405912335, '0000-00-00 00:00:00', '127.0.0.1', 42, 0, 0, '13', '0', 0, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 2, 'images/human/cloudylogo-x5.png'),
(12, '9@126.com', 'xiaohua', '062518bbbb9d381a22a3f26dab214bd8', '', '', 0, '1954-01-01', '9500.00', '0.00', 0, 0, 0, 1404787671, 1404982176, '0000-00-00 00:00:00', '127.0.0.1', 11, 0, 0, '4058', '0', 0, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 1, ''),
(13, '0@126.com', '222333', '1995716770db0828422fa852b8b68cc2', '', '', 0, '0000-00-00', '0.00', '0.00', 0, 0, 0, 1404790937, 1404790937, '0000-00-00 00:00:00', '127.0.0.1', 1, 0, 0, NULL, '0', 0, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 2, ''),
(14, '99@126.com', 'xxxxhhhh', '65b1ef82c2a561244b3147e27d4d2f5c', '', '', 0, '1954-01-01', '0.00', '0.00', 0, 0, 0, 1404793219, 1404974202, '0000-00-00 00:00:00', '127.0.0.1', 2, 0, 0, '6301', '0', 0, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 1, 'images/human/cloudy4434322.png'),
(15, '19@126.com', 'test19test', 'fd6e6278f6bd2f18b5352d6a6de487bb', '', '', 0, '1954-01-01', '0.00', '0.00', 0, 0, 0, 1404865600, 1404968549, '0000-00-00 00:00:00', '127.0.0.1', 3, 0, 0, '5586', '0', 0, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 2, 'images/human/cloudylogo-640x68.png'),
(16, 'jjkk029@126.com', 'xiaoshun', '66f6cc60d03bfdeb6e177f77ff8e8c49', '', '', 0, '1954-01-01', '9500.00', '0.00', 0, 0, 6, 1404953431, 1406281509, '0000-00-00 00:00:00', '127.0.0.1', 8, 0, 0, '7990', '0', 0, 0, '', '', '', '', '', '', 1, '0.00', NULL, NULL, 1, 'images/human/cloudylogo-60x5.png'),
(19, '966@126.com', 'fafsdfafadffasfds', '6ae696651b6da5d506c40d1f4444eca9', '', '', 1, '1954-01-01', '0.00', '0.00', 0, 0, 0, 1405308350, 1405317597, '0000-00-00 00:00:00', '127.0.0.1', 2, 0, 0, '4399', '0', 0, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 2, 'images/human/cloudy361.png'),
(20, '416633205@126.com', 'tttttxxxxx', '1995716770db0828422fa852b8b68cc2', '', '', 0, '0000-00-00', '0.00', '0.00', 0, 0, 0, 1405477005, 1405477005, '0000-00-00 00:00:00', '127.0.0.1', 1, 0, 0, NULL, '0', 0, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 0, ''),
(21, '416633205@qq.com', 'tttt8xxxx8', '1995716770db0828422fa852b8b68cc2', '', '', 0, '0000-00-00', '0.00', '0.00', 0, 0, 0, 1405477091, 1405477091, '0000-00-00 00:00:00', '127.0.0.1', 1, 0, 0, NULL, '0', 0, 0, '', '', '', '', '', '', 1, '0.00', NULL, NULL, 0, ''),
(22, 'siyecao0291986@126.com', '李世民', '31c558bc80370f4c9ecaf9f85966c494', '', '', 1, '1986-04-01', '262.00', '0.00', 466, 468, 5, 1405668216, 1410492065, '0000-00-00 00:00:00', '127.0.0.1', 108, 0, 0, '42', '0', 0, 0, '', '', '', '', '', '', 0, '0.00', 'friend_birthday', '0401', 2, 'images/human/cloudyaboutus55logo.gif'),
(24, '00000@126.com', '高圆圆', 'c3ce9c2c6006a580b1270b8d2aed9115', '', '', 0, '1954-01-01', '0.00', '0.00', 0, 0, 0, 1406792671, 1406794296, '0000-00-00 00:00:00', '127.0.0.1', 3, 0, 0, '8696', '0', 0, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 1, 'images/human/cloudy366761.png'),
(25, '99432423423@126.com', '4445555', '1995716770db0828422fa852b8b68cc2', '', '', 0, '0000-00-00', '0.00', '0.00', 0, 0, 0, 1406799656, 1406799656, '0000-00-00 00:00:00', '127.0.0.1', 1, 0, 0, NULL, '0', 0, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 0, ''),
(26, 'jjjjkkkk@126.com', '李敖', '486d18b0b2690faa3ee1bff616211774', '', '', 1, '1954-01-01', '0.00', '0.00', 0, 0, 0, 1406875464, 1408929838, '0000-00-00 00:00:00', '127.0.0.1', 3, 0, 0, '2514', '0', 0, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 1, 'images/human/cloudy34422.png'),
(27, 'jjkk02988@126.com', '李和', '1995716770db0828422fa852b8b68cc2', '', '', 0, '0000-00-00', '0.00', '0.00', 0, 0, 0, 1406876724, 1406876724, '0000-00-00 00:00:00', '127.0.0.1', 1, 0, 0, NULL, '0', 0, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 0, ''),
(28, '9999999@126.COM', '李肖', '1995716770db0828422fa852b8b68cc2', '', '', 0, '0000-00-00', '0.00', '0.00', 0, 0, 0, 1407812716, 1407812716, '0000-00-00 00:00:00', '127.0.0.1', 1, 0, 0, NULL, '0', 22, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 0, ''),
(29, 'wnn07961988@126.com', '王年', '1995716770db0828422fa852b8b68cc2', '', '', 2, '1988-01-01', '0.00', '0.00', 0, 0, 0, 1407901857, 1407901857, '0000-00-00 00:00:00', '127.0.0.1', 1, 0, 0, NULL, '0', 0, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 1, 'images/human/cloudy82324.png'),
(30, 'jack0291986@126.com', '李奥', '1995716770db0828422fa852b8b68cc2', '', '', 0, '0000-00-00', '0.00', '0.00', 0, 0, 0, 1409044724, 1409044724, '0000-00-00 00:00:00', '127.0.0.1', 1, 0, 0, NULL, '0', 0, 0, '', '', '', '', '', '', 0, '0.00', NULL, NULL, 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `fp_admins`
--

CREATE TABLE IF NOT EXISTS `fp_admins` (
  `admin_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_user_name` int(11) DEFAULT NULL,
  `admin_user_password` int(11) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `fp_admins`
--


-- --------------------------------------------------------

--
-- 表的结构 `fp_admins_role`
--

CREATE TABLE IF NOT EXISTS `fp_admins_role` (
  `admin_roles_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_parent_id` int(11) DEFAULT NULL,
  `admin_name` varchar(128) DEFAULT NULL,
  `admin_access_products` tinyint(1) NOT NULL DEFAULT '0',
  `admin_access_orders` tinyint(1) NOT NULL DEFAULT '0',
  `admin_access_sales` tinyint(1) NOT NULL DEFAULT '0',
  `admin_access_support` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`admin_roles_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `fp_admins_role`
--


-- --------------------------------------------------------

--
-- 表的结构 `fp_article`
--

CREATE TABLE IF NOT EXISTS `fp_article` (
  `article_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` smallint(5) NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL DEFAULT '',
  `content` longtext NOT NULL,
  `author` varchar(30) NOT NULL DEFAULT '',
  `author_email` varchar(60) NOT NULL DEFAULT '',
  `keywords` varchar(255) NOT NULL DEFAULT '',
  `article_type` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `is_open` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0',
  `file_url` varchar(255) NOT NULL DEFAULT '',
  `open_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `link` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`article_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- 转存表中的数据 `fp_article`
--

INSERT INTO `fp_article` (`article_id`, `cat_id`, `title`, `content`, `author`, `author_email`, `keywords`, `article_type`, `is_open`, `add_time`, `file_url`, `open_type`, `link`, `description`) VALUES
(1, 2, '免责条款', '', '', '', '', 0, 1, 1404198659, '', 0, '', NULL),
(2, 2, '隐私保护', '', '', '', '', 0, 1, 1404198659, '', 0, '', NULL),
(3, 2, '咨询热点', '', '', '', '', 0, 1, 1404198659, '', 0, '', NULL),
(4, 2, '联系我们', '', '', '', '', 0, 1, 1404198659, '', 0, '', NULL),
(5, 2, '公司简介', '', '', '', '', 0, 1, 1404198659, '', 0, '', NULL),
(6, -1, '用户协议', '', '', '', '', 0, 1, 1404198659, '', 0, '', NULL),
(7, 4, '三星电子宣布将在中国发布15款3G手机', '<p>全球领先的电子产品及第二大移动通信终端制造商三星电子今天在北京宣布，为全面支持中国开展3G移动通信业务，将在3G服务正式商用之际，向中国市场推出 15款3G手机。这些产品分别支持中国三大无线运营商的各种3G服务，并已经得到运营商的合作认可。凭借在电子和通信领域的整体技术实力和对消费者的准确 把握，三星电子已经开始全面发力中国的3G移动通信市场。<br />\n<br />\n&nbsp;&nbsp;&nbsp;&nbsp;2009年1月，中国政府宣布正式启动3G移动通信服务。并根据中国的实际情况，决定由三大运营商分别采用全部三种3G网络制式，&nbsp;即以中国自主知识产权为核心的TD-SCDMA，以欧洲为主要市场的WCDMA和源自北美的CDMA2000技术。<br />\n<br />\n&nbsp;&nbsp;&nbsp;&nbsp;多 年来，三星电子秉承&ldquo;做中国人民喜爱的企业，贡献于中国社会的企业&rdquo;的企业理念，准确地把握了中国社会的发展和市场的需求，推出了一系列深受中国消费者喜 爱的电子产品。为了配合中国推进具有自主知识产权的3G移动通信标准TD-SCDMA，&nbsp;三星电子从2000年开始在中国设立了通信技术研究院，&nbsp;开始进 行TD-SCDMA技术的研究。作为最早承诺支持中国3G标准的手机制造企业，三星电子已经先后投入了上亿元的研究费用，&nbsp;组建了几百人的研发团队。推出 的TD-SCDMA制式的产品，不仅通过了各级权威部门的试验和检测，也经历了2008年北京奥运会的现场考验。此次为中国移动定制的TD-SCDMA手 机，包括了满足高端商务需求的双待产品B7702C，以及数字电视手机、多媒体手机和时尚手机。<br />\n<br />\n&nbsp;&nbsp;&nbsp;&nbsp;作为世界第二大手机制造企业，三 星电子已经在全球3G市场积累了多年的技术和市场经验。最新的统计表明，在完全采用WCDMA标准的欧洲，三星电子的市场份额已经排名第二。在为中国联通 准备的产品中，不仅包括能够支持HSDPA的高端多媒体手机S7520U，也涵盖了能够支持高速上网、在线电影、在线音乐等适合不同消费需求的各种产品。<br />\n<br />\n&nbsp;&nbsp;&nbsp;&nbsp;而 在主要采用CDMA2000技术的北美市场，三星电子也取得了市场份额的第一名。即将陆续上市的支持中国电信3G服务的EVDO产品，有高端商务手机 W709。该机能够支持EVDO/GSM的双网双待功能，含800万像素拍摄系统。其他产品还包括音乐手机M609，双模手机W239，以及时尚设计的 F539等。<br />\n<br />\n&nbsp;&nbsp;&nbsp;&nbsp;作为世界上少数能够提供支持三种3G标准的终端厂商，三星电子正利用其在通信、半导体、显示器以及数字多媒体等方面 的优势，加快产品数字融合的进程。除上述3G手机产品外，三星电子也于近期推出了用于3G网络的上网卡和网络笔记本电脑。三星电子专务、无线事业部大中华 区高级副总裁卢基学先生说，&ldquo;上述15款新品，&nbsp;只是我们二季度新产品的一部分。随着中国3G移动通信市场的不断扩大，三星还将推出更多适合中国市场的终 端产品，以满足消费者对于通信和数字产品的不同需求。三星也将积极配合运营商业务的发展计划，加快技术和应用的研发。中国3G的移动通信市场前景将是非常 明亮的。&rdquo;</p>', '', '', '', 0, 1, 1241426864, '', 0, 'http://', NULL),
(8, 4, '诺基亚牵手移动 5款热门TD手机机型推荐', '<table width="100%" cellspacing="0" cellpadding="4" align="center" class="tableborder">\r\n<tbody>\r\n<tr>\r\n<td width="180" valign="top" class="altbg4">&nbsp;</td>\r\n<td height="100%" valign="top" class="altbg3">\r\n<table cellspacing="0" cellpadding="0" border="0" style="padding: 5px; table-layout: fixed; width: 973px; height: 2195px;">\r\n<tbody>\r\n<tr>\r\n<td valign="top">\r\n<div class="bbs_content clearfix">随着5月17日电信日的来临，自从09年初网民对于3G方面关注越来越多，目前国内3G网络主要有中国移动TD-SCDMA，中国联通WCDMA以及 中国电信的CDMA2000这三种制式。到底是哪一种网络制式能让消费者满意，相信好多消费者都难以判断。<br />\r\n<br />\r\n而作为3G网络最主要的组 成部分-手持终端（手机） ，相信也是好多消费者关注的焦点。目前，中国移动TD-SCDMA手机机型频频爆出，其中不乏亮点产品，像联想联想 Ophone、诺基亚、多普达 Magic等，下面就让笔者与大家一起来了解TD-SCDMA网络制式下的几款强势机型吧。<br />\r\n<br />\r\n诺基亚TD-SCDMA手机　型号：待定　参考报价：尚未上市<br />\r\n<br />\r\n随着国内3G网络发展速度加快及众多手机厂商纷纷跟进，诺基亚终于推出TD-SCDMA手机，这款诺基亚的首台TD-SCDMA测试手机型号目前仍无法 得知，但从键盘及菜单设计来看，我们可以是知道其并没有采用S60操作系统，只是配备了S40系统，机身直板造型与早期热卖的6300有几分相像。<br />\r\n<br />\r\n<p><img width="450" alt="" src="http://dstatic.esqimg.com/4812278/1.jpg" style="display: block;" /></p>\r\n<br />\r\n<br />\r\n图为：诺基亚TD-SCDMA手机<br />\r\n<br />\r\n虽然没有更多的参数资料，但是从曝光的图片我们可以知道这款诺基亚TD-SCDMA手机必定配备了QVGA分辨率的屏幕以及320万像素的摄像头，而标准的MP3以及蓝牙等功能自然不会缺少，在功能方面不会比以往的S40手机逊色。<br />\r\n<br />\r\n<p><img width="450" alt="" src="http://dstatic.esqimg.com/4812279/2.jpg" style="display: block;" /></p>\r\n<br />\r\n<br />\r\n图为：诺基亚TD-SCDMA手机<br />\r\n<br />\r\n这款诺基亚的TD手机最大的卖点便是提供了对TD-HSDPA技术的支持，最大的功能特色便是该技术被看为是TD网络下一步的演进技术，能够同时适用于 WCDMA和TD-SCDMA两种不同的网络支持，能够很好的支持非对称的数据业务，也就是说这款诺基亚手机的用户即便在全球漫游都能够使用到3G网络。 而其机身前置的摄像头也更是证实了其3G手机的身份。<br />\r\n<br />\r\n<p><img width="450" alt="" src="http://dstatic.esqimg.com/4812280/3.jpg" style="display: block;" /></p>\r\n<p><br />\r\n<br />\r\n图为：诺基亚TD-SCDMA手机<br />\r\n<br />\r\n从目前曝光的测试情况来看，通过这款诺基亚TD手机连接网络，其下载速度能够稳定在1.3Mbps左右，不过根据国内有些媒体的报道，诺基亚官方已经证实将于今天下半年配合运营商中国移动对出自己的第一款TD-SCDMA制式的S60手机，大家要拭目以待了。</p>\r\n<p>&nbsp;</p>\r\n图为：诺基亚TD-SCDMA手机<br />\r\n<br />\r\n最后较为耐人寻味的便是目前有业内人士指出目前曝光的的诺基亚TD手机其实是去年夏季就出现过的 TD测试手机，但是随着诺基亚拥有部分股份的TD芯片厂商&ldquo;凯明&rdquo;的倒闭，这款机型也就被取消了。尽管对于目前这款诺基亚的TD测试手机的身份尚无官方的 消息，但是诺基亚将推出TD手机遗失毫无悬念的事情了，相信大家也希望在TD制式下能够有更多的手机可以选择。</div>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>', '', '', '', 0, 1, 1241427051, '', 0, 'http://', NULL),
(9, 5, '售后流程', '', '', '', '', 0, 1, 1242576700, '', 0, 'http://', NULL),
(10, 5, '购物流程', '', '', '', '', 0, 1, 1242576717, '', 0, 'http://', NULL),
(11, 5, '订购方式', '', '', '', '', 0, 1, 1242576727, '', 0, 'http://', NULL),
(12, 6, '如何分辨原装电池', '<p><font size="2">一般在购买时主要是依靠观察外观特征的方法来鉴别电池，而原装电池一般应具有以下一些特征：&nbsp;<br />\n<br />\n1、 电池外观整齐，外表面有一定的粗糙度且手感舒适，内表面手感光滑，灯光下能看见细密的纵向划痕&nbsp;<br />\n<br />\n2、 生产厂家字样应该轮廓清晰，且防伪标志亮度高，看上去有立体感，电池标贴 字迹清晰，有与电池类型相一致的电池件号<br />\n3、 电池标贴采用二次印刷技术，在一定光线下从斜面看，条形码部分的颜色比其他部分要黑，且用手触摸有凹凸感<br />\n<br />\n4、 原装电池电极与手机电池片宽度相等，电池电极下方标有&ldquo; + &rdquo;、&ldquo; - &rdquo;标志，电池电极片之间的隔离材料与外壳材料一致，但不是一体<br />\n<br />\n5、 原装电池装入手机时手感舒适，安装自如，电池按压部分卡位适当而且牢固<br />\n<br />\n6、 原装电池的金属触点采用优质的铜片，只有在正面看时才会有反光，而从其它角度看的话，都是比较暗淡的</font></p>', '', '', '', 1, 1, 1242576826, '', 0, 'http://', NULL),
(15, 7, '货到付款区域', '', '', '', '', 0, 1, 1242577023, '', 0, 'http://', NULL),
(16, 7, '配送支付智能查询 ', '', '', '', '', 0, 1, 1242577032, '', 0, 'http://', NULL),
(17, 7, '支付方式说明', '', '', '', '', 0, 1, 1242577041, '', 0, 'http://', NULL),
(18, 10, '资金管理', '', '', '', '', 0, 1, 1242577127, '', 0, 'user.php?act=account_log', NULL),
(19, 10, '我的收藏', '', '', '', '', 0, 1, 1242577178, '', 0, 'user.php?act=collection_list', NULL),
(20, 10, '我的订单', '', '', '', '', 0, 1, 1242577199, '', 0, 'user.php?act=order_list', NULL),
(21, 8, '退换货原则', '', '', '', '服务', 0, 1, 1242577293, '', 0, 'http://', NULL),
(22, 8, '售后服务保证 ', '', '', '', '售后', 0, 1, 1242577308, '', 0, 'http://', NULL),
(23, 8, '产品质量保证 ', '', '', '', '质量', 1, 1, 1242577326, '', 0, 'http://', NULL),
(24, 9, '网站故障报告', '', '', '', '', 0, 1, 1242577432, '', 0, 'http://', NULL),
(25, 9, '选机咨询 ', '', '', '', '', 0, 1, 1242577448, '', 0, 'http://', NULL),
(26, 9, '投诉与建议 ', '', '', '', '', 0, 1, 1242577459, '', 0, 'http://', NULL),
(27, 4, '800万像素超强拍照机 LG Viewty Smart再曝光', '', '', '', '', 0, 1, 1242577702, '', 0, 'http://news.imobile.com.cn/index-a-view-id-66790.html', NULL),
(28, 11, '飞利浦9@9促销', '<p>&nbsp;</p>\r\n<div class="boxCenterList RelaArticle" id="com_v">\r\n<p align="left">作为一款性价比极高的入门级<font size="3" color="#ff0000"><strong>商务手机</strong></font>，飞利浦<a href="mailto:9@9v">Xenium&nbsp; 9@9v</a>三围大小为105&times;44&times;15.8mm，机身重量仅为<strong><font size="3" color="#ff0000">75g</font></strong>，装配了一块低规格1.75英寸128&times;160像素65000色CSTN显示屏。身正面采用月银色功能键区与屏幕数字键区相分隔，键盘设计较为<font size="3"><strong><font color="#ff0000">别</font><font color="#ff0000">致</font></strong></font>，中部导航键区采用钛金色的&ldquo;腰带&rdquo;彰显出浓郁的商务气息。</p>\r\n<p align="left">&nbsp;</p>\r\n<p align="left">此款手机采用<strong><font size="3" color="#ff0000">触摸屏</font></strong>设计，搭配精致的手写笔，可支持手写中文和英文两个版本。增强的内置系统还能识别潦草字迹，确保在移动中和匆忙时输入文字的识别率。手写指令功能还支持特定图案的瞬间调用，独特的手写记事本功能，可以在触摸屏上随意绘制个性化的图案并进行<strong><font size="3" color="#ff0000">记事提醒</font></strong>，让商务应用更加随意。</p>\r\n<p align="left">&nbsp;</p>\r\n<p align="left">&nbsp;作为入门级为数不多支持<strong><font size="3" color="#ff0000">双卡功能</font></strong>的手机，可以同时插入两张SIM卡，通过菜单随意切换，只需开启漫游自动切换模式，<a href="mailto:9@9V">9@9V</a>在该模式下能够判断网络情况，自动切换适合的手机号。</p>\r\n<p align="left">&nbsp;</p>\r\n<p align="left">&nbsp;</p>\r\n</div>\r\n<p>&nbsp;</p>', '', '', '', 0, 1, 1242578199, '', 0, 'http://', NULL),
(29, 11, '诺基亚5320 促销', '<p>&nbsp;</p>\r\n<div id="com_v" class="boxCenterList RelaArticle">\r\n<p>诺基亚5320XpressMusic音乐手机采用XpressMusic系列常见的黑红、黑蓝配色方案，而材质方便则选用的是经过<strong><font size="3" color="#ff0000">抛光处理</font></strong>的工程塑料；三围/体重为，为108&times;46&times;15mm/<strong><font size="3" color="#ff0000">90g</font></strong>，手感舒适。</p>\r\n<p>&nbsp;</p>\r\n<p>诺基亚5320采用的是一块可视面积为2.0英寸的<font size="3" color="#ff0000"><strong>1600万色</strong></font>屏幕，分辨率是常见的240&times;320像素（QVGA）。虽然屏幕不是特别大，但效果非常精细，色彩还原不错。</p>\r\n<p>&nbsp;</p>\r\n<p>手机背面，诺基亚为5320XM配备一颗<strong><font size="3" color="#ff0000">200W像素</font></strong>的摄像头，并且带有<strong><font size="3" color="#ff0000">两个LED的补光灯</font></strong>，可以实现拍照、摄像功能，并能通过彩信、邮件方式发送给朋友。</p>\r\n<p>&nbsp;</p>\r\n</div>\r\n<p>&nbsp;</p>', '', '', '', 1, 1, 1242578676, '', 0, 'http://', NULL),
(30, 11, '促销诺基亚N96', '<p>&nbsp;</p>\r\n<div class="boxCenterList RelaArticle" id="com_v">\r\n<p>诺基亚N96采用了<strong><font size="3" color="#ff0000">双向滑盖</font></strong>设计，机身整体呈灰黑色，沉稳、大气，机身材质采用了高强度的塑料材质，手机背面采用了抛光面板的设计风格。N96三维体积103*55*20mm，重量为125g。屏幕方面，诺基亚N96配备一块<strong><font size="3" color="#ff0000">2.8英寸</font></strong>的屏幕，支持<strong><font size="3" color="#ff0000">1670万色</font></strong>显示，分辨率达到QVGA（320&times;240）水准。</p>\r\n<p>&nbsp;<img src="http://img2.zol.com.cn/product/21/896/ceN6LBMCid3X6.jpg" alt="" /></p>\r\n<p>&nbsp;</p>\r\n<p>诺基亚N96设置有专门的<strong><font size="3" color="#ff0000">音乐播放键</font></strong>和标准的3.5毫米音频插口，支持多格式音乐播放。内置了<strong><font size="3" color="#ff0000">多媒体播放器</font></strong>，支持FM调频收音机等娱乐功能。N96手机支持<strong><font size="3" color="#ff0000">N-Gage游戏平台</font></strong>，内置包括<font size="3" color="#ff0000"><strong>《PinBall》完整版</strong></font>在内的四款N-Gage游戏，除了手机本身内置的游戏，还可以从N-Gage的网站下载或者购买最新的游戏，而且可以在论坛里和其他玩家一起讨论。</p>\r\n<p>&nbsp;</p>\r\n</div>\r\n<p>&nbsp;<img src="http://img2.zol.com.cn/product/21/898/cekkw57qJjSI.jpg" alt="" /></p>', '', '', '', 1, 1, 1242578826, '', 0, 'http://', NULL),
(13, 6, '如何分辨水货手机 ', '<p>\n<div class="artibody">\n<p><font size="2"><strong>1、&nbsp;什么是水货？</strong><br />\n提及水货手机，按照行业内的说法，可以将水货手机分成三类：A类、B类和C类。 </font></p>\n<p><font size="2">A类水货手机是指由国外、港澳台等地区在没有经过正常海关渠道的情况下进入国内市场的产品，就是我们常说的走私货， 与行货的主要差异是在输入法上，这类手机都是英文输入法或者是港澳台地区的繁体中文输入法。这类手机其最主要的目的是为了逃避国家关税或者因为该种产品曾 经过不正当改装而不能够通过正常渠道入关，质量一般没有大的问题。但由于逃避关税本身就是违法的，所以购买这类手机的用户根本得不到任何售后保障服务； </font></p>\n<p><font size="2">B类水货手机就是走私者将手机的系统软件由英文版升级至中文版后，偷运到内地，然后贴上非法渠道购买的入网标志，作为行货手机充数。 </font></p>\n<p><font size="2">C类水货手机则是那些由其他型号机改装、更换芯片等等方法做假&ldquo;生产&rdquo;出来的，或者就是从各地购买手机的部件，自己组装然后再贴上非法购买的入网标志。 </font></p>\n<p><font size="2">水货手机虽然不排除它是原厂正货的可能，但通过市场调研发现，绝大多数水货手机都是改版的次货，而且产品基本没有受国内厂商的保修许可。</font></p>\n<p><font size="2"><strong>2、水货有哪些？</strong>水货有两种，一种俗称港行，也称作水行，这种产品原本是在香港 及周边地区销售的，但是经过非法途径进入大陆地区销售。另一种是欧版水改机，也称作欧版，水改等，此种产品以英文改版机为主，通过刷改机内软件达到英文改 中文的目的，原来这类产品是销往欧美地区的，由于和大陆地区有相当大的价格差，所以通过走私进入中国市场。</font></p>\n<p><font size="2"><strong>3、水货手机的危害</strong><br />\n1、售后服务无保障 <br />\n手机作为精密类电子产品，软件、硬件方面都有可能产生不同的问题。购买正规渠道的手机，一旦出现问题，只要将问题反映给厂商客户服务中心并静候佳音就 可以了。大多数走私手机的贩卖点规模较小，根本没有资金和技术能力建立起自己的维修网点，因此他们往往制定非常苛刻的保修条件，将国家明令的一年保修期缩 短为三个月，并加入完全对走私手机经销商有利的诸如&ldquo;认为摔打&rdquo;等概念难以界定的排除条件(众所周知，手机很有可能发生摔撞事件)，是确确实实的霸王条 款。规定时间内手机出了故障，走私手机经销商会通过曲解条款尽可能地开脱保修责任。即使他们愿意承担保修服务，也需将手机发往广州、深圳等地，委托他人维 修。一来路途漫长，二来经手人繁多，小问题&ldquo;修&rdquo;成了大问题。最终走私手机经销商会以无法维修为由劝客户自行去当地正规客服维修。至于维修费用，他们自然 也不愿意出了。<br />\n<br />\n2、产品本身质量不过关<br />\n&nbsp;&nbsp;&nbsp; 现在很多奸商为了谋取暴利，经常使用C类的翻修或者组装手机冒充A类水货手机进行销售。作为消费者来说面对和正规行货之间巨大的价格差异，他们无法分辨想要购买的手机是否象销售商说的那样质量过硬，在销售商的巧舌如簧下只能眼看自己的钱包&ldquo;减肥&rdquo;。 </font></p>\n<p><font size="2">但是这类翻修或者组装的水货手机往往为了降低成本，其采用的配件往往也是不合格产品，甚至也是伪劣产品，可以想象由这样产品组装起来的手机的质量究竟可以好到那里去。目前在经常看到手机电池爆炸伤人的事件的报道，究其原因也是消费者购买了这些组装的水货手机。</font></p>\n<p><font size="2">而且不光这类手机硬件存在问题，包括手机使用的软件。由于组装的水货硬件规格根本无法保证和原场产品一致，手机使用的软件也会造成和手机硬件的冲突。频繁死机就是家常便饭，更有甚者会造成经常性的电话本丢失，无法联系到好友。</font></p>\n<p><br />\n<font size="2"><strong>4、如何分辨行、水货手机？</strong><br />\n1、看手机上是否贴有信息产业部&ldquo;进网许可&rdquo;标志。水货与正品的入网标志稍微有一点不同：真的入网标志一般都是针式打印机打印的，数字清晰，颜色较浅，仔细看有针打的凹痕；假的入网标志一般是普通喷墨打印机打印的，数字不很清晰，颜色较深，没有凹痕。 </font></p>\n<p><font size="2">2、检查手机的配置，包括中文说明书、电池、充电器等，如果是厂家原配，一般均贴有厂家的激光防伪标志。原厂配置的 中文说明书通常印刷精美，并与其他语言的说明书及相关产品资料的印刷质量、格式、风格等保持一致。不是原厂家配置的中文说明书通常印刷质量低劣，常出现错 别字，甚至字迹模糊。正品手机的包装盒中均附带有原厂合格证、原厂条码卡、原厂保修卡，而水货则没有。 </font></p>\n<p><font size="2">3、确认经销商的保修条例是否与厂家一致，在购买手机时应索要发票和保修卡。 </font></p>\n<p><font size="2">4、电子串号是否一致也是验证是否水货手机的重要途径。首先在手机上按&ldquo;*#06#&rdquo;，一般会在手机上显示15个数 字，这就是本手机的IMEI码。然后打开手机的电池盖，在手机里有一张贴纸，上面也有一个IMEI码，这个码应该同手机上显示的IMEI码完全一致。然后 再检查手机的外包装盒上的贴纸，上面也应该有一个IMEI码，这个码也应该同手机上显示的IMEI码完全一致。如果此三个码有不一致的地方，这个手机就有 问题。</font></p>\n</div>\n<p>&nbsp;</p>\n</p>', '', '', '', 0, 1, 1242576911, '', 0, 'http://', NULL),
(14, 6, '如何享受全国联保', '', '', '', '', 0, 1, 1242576927, '', 0, 'http://', NULL),
(31, 12, '诺基亚6681手机广告欣赏', '<object>\n<param value="always" name="allowScriptAccess" />\n<param value="transparent" name="wmode" />\n<param value="http://6.cn/player.swf?flag=0&amp;vid=nZNyu3nGNWWYjmtPQDY9nQ" name="movie" /><embed width="480" height="385" src="http://6.cn/player.swf?flag=0&amp;vid=nZNyu3nGNWWYjmtPQDY9nQ" allowscriptaccess="always" wmode="transparent" type="application/x-shockwave-flash"></embed></object>', '', '', '', 0, 1, 1242579069, '', 0, 'http://', NULL),
(32, 12, '手机游戏下载', '<p>三星SGHU308说明书下载，点击相关链接下载</p>', '', '', '', 1, 1, 1242579189, '', 0, 'http://soft.imobile.com.cn/index-a-list_softs-cid-1.html', NULL),
(33, 12, '三星SGHU308说明书下载', '<p>三星SGHU308说明书下载</p>', '', '', '', 1, 1, 1242579559, 'data/article/1245043292228851198.rar', 2, 'http://', NULL),
(34, 12, '3G知识普及', '<p>\n<h2>3G知识普及</h2>\n<div class="t_msgfont" id="postmessage_8792145"><font color="black">3G，全称为3rd Generation，中文含义就是指第三代数字通信。<br />\n</font><br />\n<font color="black">　　1995年问世的第一代<span class="t_tag" onclick="tagshow(event)" href="http://mbbs.enet.com.cn/tag.php?name=%C4%A3%C4%E2">模拟</span>制式<span class="t_tag" onclick="tagshow(event)" href="http://mbbs.enet.com.cn/tag.php?name=%CA%D6%BB%FA">手机</span>（1G）只能进行<span class="t_tag" onclick="tagshow(event)" href="http://mbbs.enet.com.cn/tag.php?name=%D3%EF%D2%F4">语音</span>通话；<br />\n</font><br />\n<font color="black">　　1996到1997年出现的第二代GSM、TDMA等数字制式手机（2G）便增加了接收数据的功能，如接收电子邮件或网页；<br />\n</font><br />\n<font color="black">　　3G不是2009年诞生的，它是上个世纪的产物，而早在2007年国外就已经产生4G了，而<span class="t_tag" onclick="tagshow(event)" href="http://mbbs.enet.com.cn/tag.php?name=%D6%D0%B9%FA">中国</span>也于2008年成功开发出<span class="t_tag" onclick="tagshow(event)" href="http://mbbs.enet.com.cn/tag.php?name=%D6%D0%B9%FA">中国</span>4G，其网络传输的速度可达到每秒钟2G，也就相当于下一部电影只要一秒钟。在上世纪90年末的日韩电影如《我的野蛮女友》中，女主角使用的可以让对方看见自己的视频<span class="t_tag" onclick="tagshow(event)" href="http://mbbs.enet.com.cn/tag.php?name=%B5%E7%BB%B0">电话</span>，就是属于3G技术的重要运用之一。日韩等国3G的运用是上世纪末期的事。而目前国外有些地区已经试运行3.5G甚至4G网络。<br />\n</font><br />\n<font color="black">　 </font><font color="black">（以下为误导）2009年问世的第三代（3G）与 前两代的主要区别是在传输声音和数据的速度上的提升，它能够在全球范围内更好地实现无缝漫游，并处理图像、音乐、视频流等多种媒体形式，提供包括网页浏 览、电话会议、电子商务等多种信息服务，同时也要考虑与已有第二代系统的良好兼容性。为了提供这种服务，无线网络必须能够支持不同的数据传输速度，也就是 说在室内、室外和行车的环境中能够分别支持至少2Mbps（兆比特／每秒）、384kbps（千比特／每秒）以及144kbps的传输速度。（此数值根据 网络环境会发生变化)。<br />\n</font><br />\n<font color="black">　　3G标准，国际电信联盟(ITU)目前一共确定了全球四大3G标准，它们分别是WCDMA、CDMA2000和TD-SCDMA和WiMAX。</font><br />\n<br />\n<font color="black">3G标准　　国际电信联盟（ITU）在2000年5月确定W-CDMA、CDMA2000、TD-SCDMA以 及WiMAX四大主流无线接口标准，写入3G技术指导性文件《2000年国际移动通讯计划》（简称IMT&mdash;2000）。 CDMA是Code Division Multiple Access (码分多址)的缩写，是第三代移动通信系统的技术基础。第一代移动通信系统采用频分多址(FDMA)的模拟调制方式，这种系统的主要缺点是频谱利用率低， 信令干扰话音业务。第二代移动通信系统主要采用时分多址(TDMA)的数字调制方式，提高了系统容量，并采用独立信道传送信令，使系统性能大大改善，但 TDMA的系统容量仍然有限，越区切换性能仍不完善。CDMA系统以其频率规划简单、系统容量大、频率复用系数高、抗多径能力强、通信质量好、软容量、软 切换等特点显示出巨大的发展潜力。下面分别介绍一下3G的几种标准：<br />\n</font><br />\n<br />\n<font color="black">　　 </font><br />\n<font color="black">(1) W-CDMA</font><font color="black"><br />\n</font><br />\n<br />\n<font color="black">　　也称为WCDMA，全称为Wideband CDMA，也称为CDMA Direct Spread，意为宽频分码多重存取，这是基于GSM网发展出来的3G技术规范，是欧洲提出的宽带CDMA技术，它与日本提出的宽带CDMA技术基本相 同，目前正在进一步融合。W-CDMA的支持者主要是以GSM系统为主的欧洲厂商，日本公司也或多或少参与其中，包括欧美的爱立信、阿尔卡特、<span class="t_tag" onclick="tagshow(event)" href="http://mbbs.enet.com.cn/tag.php?name=%C5%B5%BB%F9%D1%C7">诺基亚</span>、 朗讯、北电，以及日本的NTT、富士通、夏普等厂商。 该标准提出了GSM(2G)-GPRS-EDGE-WCDMA(3G)的演进策略。这套系统能够架设在现有的GSM网络上，对于系统提供商而言可以较轻易 地过渡，但是GSM系统相当普及的亚洲对这套新技术的接受度预料会相当高。因此W-CDMA具有先天的市场优势。<br />\n</font><br />\n<br />\n<font color="black">　　 </font><br />\n<font color="black">(2)CDMA2000</font><font color="black"><br />\n</font><br />\n<br />\n<font color="black">　　CDMA2000是由窄带CDMA(CDMA IS95)技术发展而来的宽带CDMA技术，也称为CDMA Multi-Carrier，它是由美国高通北美公司为主导提出，<span class="t_tag" onclick="tagshow(event)" href="http://mbbs.enet.com.cn/tag.php?name=%C4%A6%CD%D0%C2%DE%C0%AD">摩托罗拉</span>、Lucent 和后来加入的韩国三星都有参与，韩国现在成为该标准的主导者。这套系统是从窄频CDMAOne数字标准衍生出来的，可以从原有的CDMAOne结构直接升 级到3G，建设成本低廉。但目前使用CDMA的地区只有日、韩和北美，所以CDMA2000的支持者不如W-CDMA多。不过CDMA2000的研发技术 却是目前各标准中进度最快的，许多3G手机已经率先面世。该标准提出了从CDMA IS95(2G)-CDMA20001x-CDMA20003x(3G)的演进策略。CDMA20001x被称为2.5代移动通信技术。 CDMA20003x与CDMA20001x的主要区别在于应用了多路载波技术，通过采用三载波使带宽提高。目前<span class="t_tag" onclick="tagshow(event)" href="http://mbbs.enet.com.cn/tag.php?name=%D6%D0%B9%FA%B5%E7%D0%C5">中国电信</span>正在采用这一方案向3G过渡，并已建成了CDMA IS95网络。<br />\n</font><br />\n<br />\n<font color="black">　　 </font><br />\n<font color="black">(3)TD-SCDMA</font><font color="black"><br />\n</font><br />\n<br />\n<font color="black">　　全称为Time Division - Synchronous CDMA(时分<span class="t_tag" onclick="tagshow(event)" href="http://mbbs.enet.com.cn/tag.php?name=%CD%AC%B2%BD">同步</span>CDMA)，该标准是由中国大陆独自制定的3G标准，1999年6月29日，中国原邮电部电信科学技术研究院（大唐电信）向ITU提出。该标准将智能无线、<span class="t_tag" onclick="tagshow(event)" href="http://mbbs.enet.com.cn/tag.php?name=%CD%AC%B2%BD">同步</span>CDMA和<span class="t_tag" onclick="tagshow(event)" href="http://mbbs.enet.com.cn/tag.php?name=%C8%ED%BC%FE">软件</span>无 线电等当今国际领先技术融于其中，在频谱利用率、对业务支持具有灵活性、频率灵活性及成本等方面的独特优势。另外，由于中国内的庞大的市场，该标准受到各 大主要电信设备厂商的重视，全球一半以上的设备厂商都宣布可以支持TD&mdash;SCDMA标准。 该标准提出不经过2.5代的中间环节，直接向3G过渡，非常适用于GSM系统向3G升级。<br />\n</font><br />\n<br />\n<font color="black">　　 </font><br />\n<font color="black">(4)WiMAX</font><font color="black"><br />\n</font><br />\n<br />\n<font color="black">　　WiMAX 的全名是微波存取全球互通(Worldwide Interoperability for Microwave Access)，又称为802&middot;16无线城域网，是又一种为企业和家庭用户提供&ldquo;最后一英里&rdquo;的宽带无线连接方案。将此技术与需要授权或免授权的微波设备 相结合之后，由于成本较低，将扩大宽带无线市场，改善企业与服务供应商的认知度。2007年10月19日，国际电信联盟在日内瓦举行的无线通信全体会议 上，经过多数国家投票通过，WiMAX正式被批准成为继WCDMA、CDMA2000和TD-SCDMA之后的第四个全球3G标准。</font></div>\n</p>', '', '', '', 0, 1, 1242580013, '', 0, 'http://', NULL),
(35, 4, '“沃”的世界我做主', '<p><strong>导语：<br />\n<br />\n</strong>&nbsp;&nbsp;&nbsp;&nbsp;今年5月17日，是每年一度的世界电信日。同时，也是值得中国人民高兴的日子。昨天，中国联通企业品牌下的全品牌业务&ldquo;沃&rdquo;开始试商用，这也就意味着继中国移动、中国电信之后，国内第三种3G网络将要走入我们的生活，为我们带来更加快速便捷的通信服务。<br />\n<br />\n&nbsp;&nbsp;&nbsp;&nbsp;沃，意味着此品牌将为用户提供一个丰盈的平台，为个人客户、家庭客户、集团客户和企业服务提供全面的支撑，它代表着中国联通全新的服务理念和创新的品牌精神，在3G时代，为客户提供精彩的信息化服务。<br />\n<br />\n&nbsp;&nbsp;&nbsp;&nbsp;下面小编为各位介绍几款各大手机品牌专为&ldquo;沃&rdquo;打造的定制机型，为您迎接&ldquo;沃&rdquo;的到来做好充分准备。</p>\n<p><strong>诺基亚6210si<br />\n<br />\n</strong>&nbsp;&nbsp;&nbsp;&nbsp;诺基亚6210s大家肯定不陌生，经典的滑盖导航手机。其实6210si 与6210s外观、参数、硬件配置几乎完全一样，只不过在6210s的基础上，增加了对WCDMA网络的支持，成为中国联通定制手机。6210si采用诺 基亚经典的滑盖机身设计，机身面板为钢琴烤漆材质，高贵优雅。机身背板则为磨砂外观工程塑料材质，美观的同时增加了手机与手掌间的摩擦系数，防止使用中手 机滑落。</p>\n<p><strong>摩托罗拉A3100<br />\n</strong><br />\n&nbsp;&nbsp;&nbsp;&nbsp;作为摩托罗拉旗下为中国联通定制的A3100，它有着经典的鹅卵石造型， 大气稳重。从最初的U6，到U9再到A3100，鹅卵石的辉煌依旧。A3100有着高贵的血统，钢琴烤漆黑色面板，金属拉丝机身以及 Windows&nbsp;Mobile&nbsp;6.1&nbsp;Professional操作系统，都告诉我们它绝对是一部不可多得的好手机。</p>\n<p><br />\n<strong>三星S7520U<br />\n</strong><br />\n&nbsp;&nbsp;&nbsp;&nbsp;三星S7520U外观造型时尚，镜面设计以及超薄的 98.4&times;55&times;11.6mm金属机身，更适合女性朋友使用。通观机身，最显眼的就要数这3.0英寸的超大触摸屏幕了，400x240的WQGVA级别分 辨率，能够比QVGA级别屏幕显示更为细腻，细节表现力更强。500万像素摄像头说明了该机还是一名拍照能手，捕捉精彩瞬间不在话下。</p>', '', '', '', 0, 0, 1242974613, '', 0, 'http://', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `fp_article_cat`
--

CREATE TABLE IF NOT EXISTS `fp_article_cat` (
  `cat_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) NOT NULL DEFAULT '',
  `cat_type` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `keywords` varchar(255) NOT NULL DEFAULT '',
  `cat_desc` varchar(255) NOT NULL DEFAULT '',
  `sort_order` tinyint(3) unsigned NOT NULL DEFAULT '50',
  `show_in_nav` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `parent_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cat_id`),
  KEY `cat_type` (`cat_type`),
  KEY `sort_order` (`sort_order`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- 转存表中的数据 `fp_article_cat`
--

INSERT INTO `fp_article_cat` (`cat_id`, `cat_name`, `cat_type`, `keywords`, `cat_desc`, `sort_order`, `show_in_nav`, `parent_id`) VALUES
(1, '系统分类', 2, '', '系统保留分类', 50, 0, 0),
(2, '网店信息', 3, '', '网店信息分类', 50, 0, 1),
(3, '网店帮助分类', 4, '', '网店帮助分类', 50, 0, 1),
(4, '3G资讯', 1, '', '', 50, 0, 0),
(5, '新手上路 ', 5, '', '', 50, 0, 3),
(6, '手机常识 ', 5, '', '手机常识 ', 50, 0, 3),
(7, '配送与支付 ', 5, '', '配送与支付 ', 50, 0, 3),
(8, '服务保证 ', 5, '', '', 50, 0, 3),
(9, '联系我们 ', 5, '', '联系我们 ', 50, 0, 3),
(10, '会员中心', 5, '', '', 50, 0, 3),
(11, '手机促销', 1, '', '', 50, 0, 0),
(12, '站内快讯', 1, '', '', 50, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `fp_cards_gift`
--

CREATE TABLE IF NOT EXISTS `fp_cards_gift` (
  `card_id` int(255) NOT NULL AUTO_INCREMENT,
  `card_sn` varchar(255) NOT NULL DEFAULT '',
  `card_password` varchar(255) NOT NULL DEFAULT '',
  `end_date` datetime NOT NULL,
  `is_saled` int(255) NOT NULL DEFAULT '0',
  `add_date` datetime NOT NULL,
  PRIMARY KEY (`card_id`),
  KEY `car_sn` (`card_sn`),
  KEY `is_saled` (`is_saled`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `fp_cards_gift`
--


-- --------------------------------------------------------

--
-- 表的结构 `fp_countries`
--

CREATE TABLE IF NOT EXISTS `fp_countries` (
  `countries_id` int(11) NOT NULL AUTO_INCREMENT,
  `countries_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `countries_iso_code_2` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `countries_iso_code_3` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `address_format_id` int(11) NOT NULL,
  PRIMARY KEY (`countries_id`),
  KEY `IDX_COUNTRIES_NAME` (`countries_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=240 ;

--
-- 转存表中的数据 `fp_countries`
--

INSERT INTO `fp_countries` (`countries_id`, `countries_name`, `countries_iso_code_2`, `countries_iso_code_3`, `address_format_id`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', 1),
(2, 'Albania', 'AL', 'ALB', 1),
(3, 'Algeria', 'DZ', 'DZA', 1),
(4, 'American Samoa', 'AS', 'ASM', 1),
(5, 'Andorra', 'AD', 'AND', 1),
(6, 'Angola', 'AO', 'AGO', 1),
(7, 'Anguilla', 'AI', 'AIA', 1),
(8, 'Antarctica', 'AQ', 'ATA', 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', 1),
(10, 'Argentina', 'AR', 'ARG', 1),
(11, 'Armenia', 'AM', 'ARM', 1),
(12, 'Aruba', 'AW', 'ABW', 1),
(13, 'Australia', 'AU', 'AUS', 1),
(14, 'Austria', 'AT', 'AUT', 5),
(15, 'Azerbaijan', 'AZ', 'AZE', 1),
(16, 'Bahamas', 'BS', 'BHS', 1),
(17, 'Bahrain', 'BH', 'BHR', 1),
(18, 'Bangladesh', 'BD', 'BGD', 1),
(19, 'Barbados', 'BB', 'BRB', 1),
(20, 'Belarus', 'BY', 'BLR', 1),
(21, 'Belgium', 'BE', 'BEL', 1),
(22, 'Belize', 'BZ', 'BLZ', 1),
(23, 'Benin', 'BJ', 'BEN', 1),
(24, 'Bermuda', 'BM', 'BMU', 1),
(25, 'Bhutan', 'BT', 'BTN', 1),
(26, 'Bolivia', 'BO', 'BOL', 1),
(27, 'Bosnia and Herzegowina', 'BA', 'BIH', 1),
(28, 'Botswana', 'BW', 'BWA', 1),
(29, 'Bouvet Island', 'BV', 'BVT', 1),
(30, 'Brazil', 'BR', 'BRA', 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', 1),
(33, 'Bulgaria', 'BG', 'BGR', 1),
(34, 'Burkina Faso', 'BF', 'BFA', 1),
(35, 'Burundi', 'BI', 'BDI', 1),
(36, 'Cambodia', 'KH', 'KHM', 1),
(37, 'Cameroon', 'CM', 'CMR', 1),
(38, 'Canada', 'CA', 'CAN', 1),
(39, 'Cape Verde', 'CV', 'CPV', 1),
(40, 'Cayman Islands', 'KY', 'CYM', 1),
(41, 'Central African Republic', 'CF', 'CAF', 1),
(42, 'Chad', 'TD', 'TCD', 1),
(43, 'Chile', 'CL', 'CHL', 1),
(44, 'China', 'CN', 'CHN', 1),
(45, 'Christmas Island', 'CX', 'CXR', 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', 1),
(47, 'Colombia', 'CO', 'COL', 1),
(48, 'Comoros', 'KM', 'COM', 1),
(49, 'Congo', 'CG', 'COG', 1),
(50, 'Cook Islands', 'CK', 'COK', 1),
(51, 'Costa Rica', 'CR', 'CRI', 1),
(52, 'Cote D''Ivoire', 'CI', 'CIV', 1),
(53, 'Croatia', 'HR', 'HRV', 1),
(54, 'Cuba', 'CU', 'CUB', 1),
(55, 'Cyprus', 'CY', 'CYP', 1),
(56, 'Czech Republic', 'CZ', 'CZE', 1),
(57, 'Denmark', 'DK', 'DNK', 1),
(58, 'Djibouti', 'DJ', 'DJI', 1),
(59, 'Dominica', 'DM', 'DMA', 1),
(60, 'Dominican Republic', 'DO', 'DOM', 1),
(61, 'East Timor', 'TP', 'TMP', 1),
(62, 'Ecuador', 'EC', 'ECU', 1),
(63, 'Egypt', 'EG', 'EGY', 1),
(64, 'El Salvador', 'SV', 'SLV', 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', 1),
(66, 'Eritrea', 'ER', 'ERI', 1),
(67, 'Estonia', 'EE', 'EST', 1),
(68, 'Ethiopia', 'ET', 'ETH', 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', 1),
(70, 'Faroe Islands', 'FO', 'FRO', 1),
(71, 'Fiji', 'FJ', 'FJI', 1),
(72, 'Finland', 'FI', 'FIN', 1),
(73, 'France', 'FR', 'FRA', 1),
(74, 'France, Metropolitan', 'FX', 'FXX', 1),
(75, 'French Guiana', 'GF', 'GUF', 1),
(76, 'French Polynesia', 'PF', 'PYF', 1),
(77, 'French Southern Territories', 'TF', 'ATF', 1),
(78, 'Gabon', 'GA', 'GAB', 1),
(79, 'Gambia', 'GM', 'GMB', 1),
(80, 'Georgia', 'GE', 'GEO', 1),
(81, 'Germany', 'DE', 'DEU', 5),
(82, 'Ghana', 'GH', 'GHA', 1),
(83, 'Gibraltar', 'GI', 'GIB', 1),
(84, 'Greece', 'GR', 'GRC', 1),
(85, 'Greenland', 'GL', 'GRL', 1),
(86, 'Grenada', 'GD', 'GRD', 1),
(87, 'Guadeloupe', 'GP', 'GLP', 1),
(88, 'Guam', 'GU', 'GUM', 1),
(89, 'Guatemala', 'GT', 'GTM', 1),
(90, 'Guinea', 'GN', 'GIN', 1),
(91, 'Guinea-bissau', 'GW', 'GNB', 1),
(92, 'Guyana', 'GY', 'GUY', 1),
(93, 'Haiti', 'HT', 'HTI', 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', 1),
(95, 'Honduras', 'HN', 'HND', 1),
(96, 'Hong Kong', 'HK', 'HKG', 1),
(97, 'Hungary', 'HU', 'HUN', 1),
(98, 'Iceland', 'IS', 'ISL', 1),
(99, 'India', 'IN', 'IND', 1),
(100, 'Indonesia', 'ID', 'IDN', 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', 1),
(102, 'Iraq', 'IQ', 'IRQ', 1),
(103, 'Ireland', 'IE', 'IRL', 1),
(104, 'Israel', 'IL', 'ISR', 1),
(105, 'Italy', 'IT', 'ITA', 1),
(106, 'Jamaica', 'JM', 'JAM', 1),
(107, 'Japan', 'JP', 'JPN', 1),
(108, 'Jordan', 'JO', 'JOR', 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', 1),
(110, 'Kenya', 'KE', 'KEN', 1),
(111, 'Kiribati', 'KI', 'KIR', 1),
(112, 'Korea, Democratic People''s Republic of', 'KP', 'PRK', 1),
(113, 'Korea, Republic of', 'KR', 'KOR', 1),
(114, 'Kuwait', 'KW', 'KWT', 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', 1),
(116, 'Lao People''s Democratic Republic', 'LA', 'LAO', 1),
(117, 'Latvia', 'LV', 'LVA', 1),
(118, 'Lebanon', 'LB', 'LBN', 1),
(119, 'Lesotho', 'LS', 'LSO', 1),
(120, 'Liberia', 'LR', 'LBR', 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', 1),
(122, 'Liechtenstein', 'LI', 'LIE', 1),
(123, 'Lithuania', 'LT', 'LTU', 1),
(124, 'Luxembourg', 'LU', 'LUX', 1),
(125, 'Macau', 'MO', 'MAC', 1),
(126, 'Macedonia, The Former Yugoslav Republic of', 'MK', 'MKD', 1),
(127, 'Madagascar', 'MG', 'MDG', 1),
(128, 'Malawi', 'MW', 'MWI', 1),
(129, 'Malaysia', 'MY', 'MYS', 1),
(130, 'Maldives', 'MV', 'MDV', 1),
(131, 'Mali', 'ML', 'MLI', 1),
(132, 'Malta', 'MT', 'MLT', 1),
(133, 'Marshall Islands', 'MH', 'MHL', 1),
(134, 'Martinique', 'MQ', 'MTQ', 1),
(135, 'Mauritania', 'MR', 'MRT', 1),
(136, 'Mauritius', 'MU', 'MUS', 1),
(137, 'Mayotte', 'YT', 'MYT', 1),
(138, 'Mexico', 'MX', 'MEX', 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', 1),
(140, 'Moldova, Republic of', 'MD', 'MDA', 1),
(141, 'Monaco', 'MC', 'MCO', 1),
(142, 'Mongolia', 'MN', 'MNG', 1),
(143, 'Montserrat', 'MS', 'MSR', 1),
(144, 'Morocco', 'MA', 'MAR', 1),
(145, 'Mozambique', 'MZ', 'MOZ', 1),
(146, 'Myanmar', 'MM', 'MMR', 1),
(147, 'Namibia', 'NA', 'NAM', 1),
(148, 'Nauru', 'NR', 'NRU', 1),
(149, 'Nepal', 'NP', 'NPL', 1),
(150, 'Netherlands', 'NL', 'NLD', 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', 1),
(152, 'New Caledonia', 'NC', 'NCL', 1),
(153, 'New Zealand', 'NZ', 'NZL', 1),
(154, 'Nicaragua', 'NI', 'NIC', 1),
(155, 'Niger', 'NE', 'NER', 1),
(156, 'Nigeria', 'NG', 'NGA', 1),
(157, 'Niue', 'NU', 'NIU', 1),
(158, 'Norfolk Island', 'NF', 'NFK', 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', 1),
(160, 'Norway', 'NO', 'NOR', 1),
(161, 'Oman', 'OM', 'OMN', 1),
(162, 'Pakistan', 'PK', 'PAK', 1),
(163, 'Palau', 'PW', 'PLW', 1),
(164, 'Panama', 'PA', 'PAN', 1),
(165, 'Papua New Guinea', 'PG', 'PNG', 1),
(166, 'Paraguay', 'PY', 'PRY', 1),
(167, 'Peru', 'PE', 'PER', 1),
(168, 'Philippines', 'PH', 'PHL', 1),
(169, 'Pitcairn', 'PN', 'PCN', 1),
(170, 'Poland', 'PL', 'POL', 1),
(171, 'Portugal', 'PT', 'PRT', 1),
(172, 'Puerto Rico', 'PR', 'PRI', 1),
(173, 'Qatar', 'QA', 'QAT', 1),
(174, 'Reunion', 'RE', 'REU', 1),
(175, 'Romania', 'RO', 'ROM', 1),
(176, 'Russian Federation', 'RU', 'RUS', 1),
(177, 'Rwanda', 'RW', 'RWA', 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', 1),
(179, 'Saint Lucia', 'LC', 'LCA', 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', 1),
(181, 'Samoa', 'WS', 'WSM', 1),
(182, 'San Marino', 'SM', 'SMR', 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', 1),
(184, 'Saudi Arabia', 'SA', 'SAU', 1),
(185, 'Senegal', 'SN', 'SEN', 1),
(186, 'Seychelles', 'SC', 'SYC', 1),
(187, 'Sierra Leone', 'SL', 'SLE', 1),
(188, 'Singapore', 'SG', 'SGP', 4),
(189, 'Slovakia (Slovak Republic)', 'SK', 'SVK', 1),
(190, 'Slovenia', 'SI', 'SVN', 1),
(191, 'Solomon Islands', 'SB', 'SLB', 1),
(192, 'Somalia', 'SO', 'SOM', 1),
(193, 'South Africa', 'ZA', 'ZAF', 1),
(194, 'South Georgia and the South Sandwich Islands', 'GS', 'SGS', 1),
(195, 'Spain', 'ES', 'ESP', 3),
(196, 'Sri Lanka', 'LK', 'LKA', 1),
(197, 'St. Helena', 'SH', 'SHN', 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', 1),
(199, 'Sudan', 'SD', 'SDN', 1),
(200, 'Suriname', 'SR', 'SUR', 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', 1),
(202, 'Swaziland', 'SZ', 'SWZ', 1),
(203, 'Sweden', 'SE', 'SWE', 1),
(204, 'Switzerland', 'CH', 'CHE', 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', 1),
(206, 'Taiwan', 'TW', 'TWN', 1),
(207, 'Tajikistan', 'TJ', 'TJK', 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', 1),
(209, 'Thailand', 'TH', 'THA', 1),
(210, 'Togo', 'TG', 'TGO', 1),
(211, 'Tokelau', 'TK', 'TKL', 1),
(212, 'Tonga', 'TO', 'TON', 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', 1),
(214, 'Tunisia', 'TN', 'TUN', 1),
(215, 'Turkey', 'TR', 'TUR', 1),
(216, 'Turkmenistan', 'TM', 'TKM', 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', 1),
(218, 'Tuvalu', 'TV', 'TUV', 1),
(219, 'Uganda', 'UG', 'UGA', 1),
(220, 'Ukraine', 'UA', 'UKR', 1),
(221, 'United Arab Emirates', 'AE', 'ARE', 1),
(222, 'United Kingdom', 'GB', 'GBR', 1),
(223, 'United States', 'US', 'USA', 2),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', 1),
(225, 'Uruguay', 'UY', 'URY', 1),
(226, 'Uzbekistan', 'UZ', 'UZB', 1),
(227, 'Vanuatu', 'VU', 'VUT', 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', 1),
(229, 'Venezuela', 'VE', 'VEN', 1),
(230, 'Viet Nam', 'VN', 'VNM', 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', 1),
(234, 'Western Sahara', 'EH', 'ESH', 1),
(235, 'Yemen', 'YE', 'YEM', 1),
(236, 'Yugoslavia', 'YU', 'YUG', 1),
(237, 'Zaire', 'ZR', 'ZAR', 1),
(238, 'Zambia', 'ZM', 'ZMB', 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', 1);

-- --------------------------------------------------------

--
-- 表的结构 `fp_currencies`
--

CREATE TABLE IF NOT EXISTS `fp_currencies` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `symbol_left` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `symbol_right` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `decimal_point` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thousands_point` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `decimal_places` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` float(13,8) DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`currency_id`),
  KEY `idx_currencies_code` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `fp_currencies`
--

INSERT INTO `fp_currencies` (`currency_id`, `code`, `symbol_left`, `symbol_right`, `decimal_point`, `thousands_point`, `decimal_places`, `value`, `last_updated`) VALUES
(1, 'USD', '$', '', '.', ',', '2', 1.00000000, '2014-09-12 16:26:27'),
(2, 'EUR', '', '€', '.', ',', '2', 1.00000000, '2014-09-12 16:26:27'),
(3, 'CNY', '', '￥', '.', ',', '2', 1.00000000, '2014-09-12 16:26:27');

-- --------------------------------------------------------

--
-- 表的结构 `fp_currencies_titles`
--

CREATE TABLE IF NOT EXISTS `fp_currencies_titles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `currency_id` int(11) NOT NULL,
  `languages_id` int(11) NOT NULL,
  `currency_title` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- 转存表中的数据 `fp_currencies_titles`
--

INSERT INTO `fp_currencies_titles` (`id`, `currency_id`, `languages_id`, `currency_title`) VALUES
(1, 1, 1, 'U.S. Dollar'),
(2, 2, 1, 'Euro'),
(4, 1, 2, '美元'),
(5, 2, 2, '欧元'),
(3, 3, 1, 'Chinese Yuan'),
(6, 3, 2, '人民币');

-- --------------------------------------------------------

--
-- 表的结构 `fp_languages`
--

CREATE TABLE IF NOT EXISTS `fp_languages` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `language_code` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `language_image` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language_directory` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language_sort_order` int(3) DEFAULT NULL,
  PRIMARY KEY (`language_id`),
  KEY `IDX_LANGUAGES_NAME` (`language_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `fp_languages`
--

INSERT INTO `fp_languages` (`language_id`, `language_name`, `language_code`, `language_image`, `language_directory`, `language_sort_order`) VALUES
(1, 'English', 'en', 'icon.gif', 'english', 1),
(2, 'Simple Chinese', 'cn', 'icon-cn.gif', 'simple_chinese', 2);

-- --------------------------------------------------------

--
-- 表的结构 `fp_messages`
--

CREATE TABLE IF NOT EXISTS `fp_messages` (
  `message_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL DEFAULT '0',
  `message_parent_id` varchar(255) NOT NULL DEFAULT '0',
  `message_user_name` varchar(255) NOT NULL DEFAULT '',
  `message_user_email` varchar(255) NOT NULL DEFAULT '',
  `message_msg_title` varchar(255) NOT NULL DEFAULT '',
  `message_type` int(255) unsigned NOT NULL DEFAULT '0',
  `message_status` int(255) unsigned NOT NULL DEFAULT '0',
  `message_content` varchar(255) NOT NULL,
  `message_time` datetime NOT NULL,
  `message_img` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`message_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `fp_messages`
--


-- --------------------------------------------------------

--
-- 表的结构 `fp_newsletters`
--

CREATE TABLE IF NOT EXISTS `fp_newsletters` (
  `newsletters_id` varchar(255) NOT NULL,
  `newsletters_email` varchar(255) NOT NULL DEFAULT '',
  `newsletters_status` tinyint(255) NOT NULL DEFAULT '0',
  `newsletters_hash` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`newsletters_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `fp_newsletters`
--


-- --------------------------------------------------------

--
-- 表的结构 `fp_newsletter_template`
--

CREATE TABLE IF NOT EXISTS `fp_newsletter_template` (
  `template_id` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `template_code` varchar(30) NOT NULL DEFAULT '',
  `is_html` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `template_subject` varchar(200) NOT NULL DEFAULT '',
  `template_content` text NOT NULL,
  `last_modify` int(10) unsigned NOT NULL DEFAULT '0',
  `last_send` int(10) unsigned NOT NULL DEFAULT '0',
  `type` varchar(10) NOT NULL,
  PRIMARY KEY (`template_id`),
  UNIQUE KEY `template_code` (`template_code`),
  KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- 转存表中的数据 `fp_newsletter_template`
--

INSERT INTO `fp_newsletter_template` (`template_id`, `template_code`, `is_html`, `template_subject`, `template_content`, `last_modify`, `last_send`, `type`) VALUES
(1, 'send_password', 1, '密码找回', '{$user_name}您好！<br>\n<br>\n您已经进行了密码重置的操作，请点击以下链接(或者复制到您的浏览器):<br>\n<br>\n<a href="{$reset_email}" target="_blank">{$reset_email}</a><br>\n<br>\n以确认您的新密码重置操作！<br>\n<br>\n{$shop_name}<br>\n{$send_date}', 1194824789, 0, 'template'),
(2, 'order_confirm', 0, '订单确认通知', '亲爱的{$order.consignee}，你好！ \n\n我们已经收到您于 {$order.formated_add_time} 提交的订单，该订单编号为：{$order.order_sn} 请记住这个编号以便日后的查询。\n\n{$shop_name}\n{$sent_date}\n\n\n', 1158226370, 0, 'template'),
(3, 'deliver_notice', 1, '发货通知', '亲爱的{$order.consignee}。你好！</br></br>\n\n您的订单{$order.order_sn}已于{$send_time}按照您预定的配送方式给您发货了。</br>\n</br>\n{if $order.invoice_no}发货单号是{$order.invoice_no}。</br>{/if}\n</br>\n在您收到货物之后请点击下面的链接确认您已经收到货物：</br>\n<a href="{$confirm_url}" target="_blank">{$confirm_url}</a></br></br>\n如果您还没有收到货物可以点击以下链接给我们留言：</br></br>\n<a href="{$send_msg_url}" target="_blank">{$send_msg_url}</a></br>\n<br>\n再次感谢您对我们的支持。欢迎您的再次光临。 <br>\n<br>\n{$shop_name} </br>\n{$send_date}', 1194823291, 0, 'template'),
(4, 'order_cancel', 0, '订单取消', '亲爱的{$order.consignee}，你好！ \n\n您的编号为：{$order.order_sn}的订单已取消。\n\n{$shop_name}\n{$send_date}', 1156491130, 0, 'template'),
(5, 'order_invalid', 0, '订单无效', '亲爱的{$order.consignee}，你好！\n\n您的编号为：{$order.order_sn}的订单无效。\n\n{$shop_name}\n{$send_date}', 1156491164, 0, 'template'),
(6, 'send_bonus', 0, '发红包', '亲爱的{$user_name}您好！\n\n恭喜您获得了{$count}个红包，金额{if $count > 1}分别{/if}为{$money}\n\n{$shop_name}\n{$send_date}\n', 1156491184, 0, 'template'),
(7, 'group_buy', 1, '团购商品', '亲爱的{$consignee}，您好！<br>\n<br>\n您于{$order_time}在本店参加团购商品活动，所购买的商品名称为：{$goods_name}，数量：{$goods_number}，订单号为：{$order_sn}，订单金额为：{$order_amount}<br>\n<br>\n此团购商品现在已到结束日期，并达到最低价格，您现在可以对该订单付款。<br>\n<br>\n请点击下面的链接：<br>\n<a href="{$shop_url}" target="_blank">{$shop_url}</a><br>\n<br>\n请尽快登录到用户中心，查看您的订单详情信息。 <br>\n<br>\n{$shop_name} <br>\n<br>\n{$send_date}', 1194824668, 0, 'template'),
(8, 'register_validate', 1, '邮件验证', '{$user_name}您好！<br><br>\r\n\r\n这封邮件是 {$shop_name} 发送的。你收到这封邮件是为了验证你注册邮件地址是否有效。如果您已经通过验证了，请忽略这封邮件。<br>\r\n请点击以下链接(或者复制到您的浏览器)来验证你的邮件地址:<br>\r\n<a href="{$validate_email}" target="_blank">{$validate_email}</a><br><br>\r\n\r\n{$shop_name}<br>\r\n{$send_date}', 1162201031, 0, 'template'),
(9, 'virtual_card', 0, '虚拟卡片', '亲爱的{$order.consignee}\r\n你好！您的订单{$order.order_sn}中{$goods.goods_name} 商品的详细信息如下:\r\n{foreach from=$virtual_card item=card}\r\n{if $card.card_sn}卡号：{$card.card_sn}{/if}{if $card.card_password}卡片密码：{$card.card_password}{/if}{if $card.end_date}截至日期：{$card.end_date}{/if}\r\n{/foreach}\r\n再次感谢您对我们的支持。欢迎您的再次光临。\r\n\r\n{$shop_name} \r\n{$send_date}', 1162201031, 0, 'template'),
(10, 'attention_list', 0, '关注商品', '亲爱的{$user_name}您好~\n\n您关注的商品 : {$goods_name} 最近已经更新,请您查看最新的商品信息\n\n{$goods_url}\r\n\r\n{$shop_name} \r\n{$send_date}', 1183851073, 0, 'template'),
(11, 'remind_of_new_order', 0, '新订单通知', '亲爱的店长，您好：\n   快来看看吧，又有新订单了。\n    订单号:{$order.order_sn} \n 订单金额:{$order.order_amount}，\n 用户购买商品:{foreach from=$goods_list item=goods_data}{$goods_data.goods_name}(货号:{$goods_data.goods_sn})    {/foreach} \n\n 收货人:{$order.consignee}， \n 收货人地址:{$order.address}，\n 收货人电话:{$order.tel} {$order.mobile}, \n 配送方式:{$order.shipping_name}(费用:{$order.shipping_fee}), \n 付款方式:{$order.pay_name}(费用:{$order.pay_fee})。\n\n               系统提醒\n               {$send_date}', 1196239170, 0, 'template'),
(12, 'goods_booking', 1, '缺货回复', '亲爱的{$user_name}。你好！</br></br>{$dispose_note}</br></br>您提交的缺货商品链接为</br></br><a href="{$goods_link}" target="_blank">{$goods_name}</a></br><br>{$shop_name} </br>{$send_date}', 0, 0, 'template'),
(13, 'user_message', 1, '留言回复', '亲爱的{$user_name}。你好！</br></br>对您的留言：</br>{$message_content}</br></br>店主作了如下回复：</br>{$message_note}</br></br>您可以随时回到店中和店主继续沟通。</br>{$shop_name}</br>{$send_date}', 0, 0, 'template'),
(14, 'recomment', 1, '用户评论回复', '亲爱的{$user_name}。你好！</br></br>对您的评论：</br>“{$comment}”</br></br>店主作了如下回复：</br>“{$recomment}”</br></br>您可以随时回到店中和店主继续沟通。</br>{$shop_name}</br>{$send_date}', 0, 0, 'template');

-- --------------------------------------------------------

--
-- 表的结构 `fp_orders`
--

CREATE TABLE IF NOT EXISTS `fp_orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_street_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_suburb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_country` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_email_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `delivery_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_street_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_suburb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_address_format_id` int(5) NOT NULL,
  `billing_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_street_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_suburb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billing_address_format_id` int(5) NOT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `credit_card_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `credit_card_owner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `credit_card_number` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `credit_card_expires` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_purchased` datetime DEFAULT NULL,
  `orders_status_id` int(5) NOT NULL,
  `orders_date_finished` datetime DEFAULT NULL,
  `currency_id` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_value` decimal(14,6) DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `idx_orders_customers_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `fp_orders`
--


-- --------------------------------------------------------

--
-- 表的结构 `fp_orders_serialnumers`
--

CREATE TABLE IF NOT EXISTS `fp_orders_serialnumers` (
  `serialnumers_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `user_id` int(128) unsigned DEFAULT NULL,
  `user_email` varchar(128) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `serialnumers_code` varchar(256) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `serialnumers_is_valid` tinyint(1) unsigned NOT NULL,
  `serialnumers_devices_count` tinyint(1) unsigned NOT NULL,
  `serialnumers_devices_count_max` tinyint(1) unsigned NOT NULL,
  `serialnumers_create_date` datetime NOT NULL,
  PRIMARY KEY (`serialnumers_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `fp_orders_serialnumers`
--


-- --------------------------------------------------------

--
-- 表的结构 `fp_orders_status`
--

CREATE TABLE IF NOT EXISTS `fp_orders_status` (
  `orders_status_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '1',
  `orders_status_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `public_flag` int(11) DEFAULT '1',
  `downloads_flag` int(11) DEFAULT '0',
  PRIMARY KEY (`orders_status_id`,`language_id`),
  KEY `idx_orders_status_name` (`orders_status_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `fp_orders_status`
--

INSERT INTO `fp_orders_status` (`orders_status_id`, `language_id`, `orders_status_name`, `public_flag`, `downloads_flag`) VALUES
(1, 1, 'Pending', 1, 0),
(2, 1, 'Processing', 1, 1),
(3, 1, 'Delivered', 1, 1),
(4, 1, 'PayPal [Transactions]', 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `fp_orders_subscription`
--

CREATE TABLE IF NOT EXISTS `fp_orders_subscription` (
  `subscription_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `user_id` int(128) unsigned NOT NULL,
  `user_email` varchar(128) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `subscription_start_time` datetime NOT NULL,
  `subscription_end_time` datetime NOT NULL,
  `subscription_auto_renew` tinyint(1) NOT NULL DEFAULT '1',
  `subscription_is_valid` tinyint(1) NOT NULL DEFAULT '1',
  `subscription_devices_count` tinyint(1) NOT NULL DEFAULT '1',
  `subscription_devices_max` tinyint(1) NOT NULL DEFAULT '1',
  `subscription_create_date` datetime NOT NULL,
  PRIMARY KEY (`subscription_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `fp_orders_subscription`
--


-- --------------------------------------------------------

--
-- 表的结构 `fp_payments`
--

CREATE TABLE IF NOT EXISTS `fp_payments` (
  `payment_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `payment_code` varchar(20) NOT NULL DEFAULT '',
  `payment_name` varchar(120) NOT NULL DEFAULT '',
  `payment_fee` varchar(10) NOT NULL DEFAULT '0',
  `payment_description` text NOT NULL,
  `payment_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `payment_configure` text NOT NULL,
  `payment_enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `payment_is_cod` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `payment_is_online` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`payment_id`),
  UNIQUE KEY `pay_code` (`payment_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `fp_payments`
--


-- --------------------------------------------------------

--
-- 表的结构 `fp_payments_log`
--

CREATE TABLE IF NOT EXISTS `fp_payments_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `order_amount` decimal(10,2) unsigned NOT NULL,
  `order_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_paid` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `fp_payments_log`
--


-- --------------------------------------------------------

--
-- 表的结构 `fp_products`
--

CREATE TABLE IF NOT EXISTS `fp_products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_quantity` int(4) NOT NULL,
  `product_model` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_icon` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_date_added` datetime NOT NULL,
  `product_last_modified` datetime DEFAULT NULL,
  `product_date_available` datetime DEFAULT NULL,
  `product_weight` decimal(5,2) NOT NULL,
  `product_status` tinyint(1) NOT NULL,
  `products_ordered` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`),
  KEY `idx_products_model` (`product_model`),
  KEY `idx_products_date_added` (`product_date_added`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- 转存表中的数据 `fp_products`
--

INSERT INTO `fp_products` (`product_id`, `product_quantity`, `product_model`, `product_icon`, `product_date_added`, `product_last_modified`, `product_date_available`, `product_weight`, `product_status`, `products_ordered`) VALUES
(1, 32, 'MG200MMS', 'matrox/mg200mms.gif', '2014-09-12 16:26:27', NULL, NULL, '23.00', 1, 0),
(2, 32, 'MG400-32MB', 'matrox/mg400-32mb.gif', '2014-09-12 16:26:27', NULL, NULL, '23.00', 1, 0),
(3, 2, 'MSIMPRO', 'microsoft/msimpro.gif', '2014-09-12 16:26:27', NULL, NULL, '7.00', 1, 0),
(4, 13, 'DVD-RPMK', 'dvd/replacement_killers.gif', '2014-09-12 16:26:27', NULL, NULL, '23.00', 1, 0),
(5, 17, 'DVD-BLDRNDC', 'dvd/blade_runner.gif', '2014-09-12 16:26:27', NULL, NULL, '7.00', 1, 0),
(6, 10, 'DVD-MATR', 'dvd/the_matrix.gif', '2014-09-12 16:26:27', NULL, NULL, '7.00', 1, 0),
(7, 10, 'DVD-YGEM', 'dvd/youve_got_mail.gif', '2014-09-12 16:26:27', NULL, NULL, '7.00', 1, 0),
(8, 10, 'DVD-ABUG', 'dvd/a_bugs_life.gif', '2014-09-12 16:26:27', NULL, NULL, '7.00', 1, 0),
(9, 10, 'DVD-UNSG', 'dvd/under_siege.gif', '2014-09-12 16:26:27', NULL, NULL, '7.00', 1, 0),
(10, 10, 'DVD-UNSG2', 'dvd/under_siege2.gif', '2014-09-12 16:26:27', NULL, NULL, '7.00', 1, 0),
(11, 10, 'DVD-FDBL', 'dvd/fire_down_below.gif', '2014-09-12 16:26:27', NULL, NULL, '7.00', 1, 0),
(12, 10, 'DVD-DHWV', 'dvd/die_hard_3.gif', '2014-09-12 16:26:27', NULL, NULL, '7.00', 1, 0),
(13, 10, 'DVD-LTWP', 'dvd/lethal_weapon.gif', '2014-09-12 16:26:27', NULL, NULL, '7.00', 1, 0),
(14, 10, 'DVD-REDC', 'dvd/red_corner.gif', '2014-09-12 16:26:27', NULL, NULL, '7.00', 1, 0),
(15, 10, 'DVD-FRAN', 'dvd/frantic.gif', '2014-09-12 16:26:27', NULL, NULL, '7.00', 1, 0),
(16, 10, 'DVD-CUFI', 'dvd/courage_under_fire.gif', '2014-09-12 16:26:27', NULL, NULL, '7.00', 1, 0),
(17, 10, 'DVD-SPEED', 'dvd/speed.gif', '2014-09-12 16:26:27', NULL, NULL, '7.00', 1, 0),
(18, 10, 'DVD-SPEED2', 'dvd/speed_2.gif', '2014-09-12 16:26:27', NULL, NULL, '7.00', 1, 0),
(19, 10, 'DVD-TSAB', 'dvd/theres_something_about_mary.gif', '2014-09-12 16:26:27', NULL, NULL, '7.00', 1, 0),
(20, 10, 'DVD-BELOVED', 'dvd/beloved.gif', '2014-09-12 16:26:27', NULL, NULL, '7.00', 1, 0),
(21, 16, 'PC-SWAT3', 'sierra/swat_3.gif', '2014-09-12 16:26:27', NULL, NULL, '7.00', 1, 0),
(22, 13, 'PC-UNTM', 'gt_interactive/unreal_tournament.gif', '2014-09-12 16:26:27', NULL, NULL, '7.00', 1, 0),
(23, 16, 'PC-TWOF', 'gt_interactive/wheel_of_time.gif', '2014-09-12 16:26:27', NULL, NULL, '10.00', 1, 0),
(24, 17, 'PC-DISC', 'gt_interactive/disciples.gif', '2014-09-12 16:26:27', NULL, NULL, '8.00', 1, 0),
(25, 16, 'MSINTKB', 'microsoft/intkeyboardps2.gif', '2014-09-12 16:26:27', NULL, NULL, '8.00', 1, 0),
(26, 10, 'MSIMEXP', 'microsoft/imexplorer.gif', '2014-09-12 16:26:27', NULL, NULL, '8.00', 1, 0),
(27, 8, 'HPLJ1100XI', 'hewlett_packard/lj1100xi.gif', '2014-09-12 16:26:27', NULL, NULL, '45.00', 1, 0),
(28, 100, 'GT-P1000', 'samsung/galaxy_tab.gif', '2014-09-12 16:26:27', NULL, NULL, '1.00', 1, 0);

-- --------------------------------------------------------

--
-- 表的结构 `fp_products_categories`
--

CREATE TABLE IF NOT EXISTS `fp_products_categories` (
  `category_id` mediumint(11) NOT NULL AUTO_INCREMENT,
  `category_image` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(3) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  KEY `idx_categories_parent_id` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `fp_products_categories`
--

INSERT INTO `fp_products_categories` (`category_id`, `category_image`, `parent_id`, `sort_order`, `date_added`, `last_modified`) VALUES
(1, 'category_createPDF.gif', 0, 1, '2014-09-12 16:26:27', NULL),
(2, 'category_editPDF.gif', 0, 2, '2014-09-12 16:26:27', NULL),
(3, 'category_exportPDF.gif', 0, 3, '2014-09-12 16:26:27', NULL),
(4, 'category_cloudPDF.gif', 0, 4, '2014-09-12 16:26:27', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `fp_products_categories_descriptions`
--

CREATE TABLE IF NOT EXISTS `fp_products_categories_descriptions` (
  `category_descripiton_id` mediumint(11) NOT NULL DEFAULT '0',
  `language_id` mediumint(11) NOT NULL DEFAULT '1',
  `category_descripiton_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`category_descripiton_id`,`language_id`),
  KEY `idx_categories_name` (`category_descripiton_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `fp_products_categories_descriptions`
--

INSERT INTO `fp_products_categories_descriptions` (`category_descripiton_id`, `language_id`, `category_descripiton_name`) VALUES
(1, 1, 'Create PDF'),
(2, 1, 'Edit PDF'),
(8, 2, '云端PDF'),
(7, 2, '导出PDF'),
(6, 2, '编辑PDF'),
(5, 2, '创建PDF'),
(3, 1, 'Export PDF'),
(4, 1, 'Cloud PDF');

-- --------------------------------------------------------

--
-- 表的结构 `fp_products_descriptions`
--

CREATE TABLE IF NOT EXISTS `fp_products_descriptions` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `os_device_id` int(11) NOT NULL,
  `product_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `product_brief` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `product_keyword` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `product_description` text COLLATE utf8_unicode_ci,
  `product_is_new` tinyint(1) DEFAULT '0',
  `product_is_hot` tinyint(1) DEFAULT '0',
  `product_is_promote` tinyint(1) DEFAULT '0',
  `product_is_favorite` tinyint(1) DEFAULT '0',
  `product_is_delete` tinyint(1) DEFAULT '0',
  `product_viewed` int(5) DEFAULT '0',
  PRIMARY KEY (`product_id`,`language_id`),
  KEY `products_name` (`product_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- 转存表中的数据 `fp_products_descriptions`
--

INSERT INTO `fp_products_descriptions` (`product_id`, `category_id`, `language_id`, `os_device_id`, `product_name`, `product_brief`, `product_keyword`, `product_description`, `product_is_new`, `product_is_hot`, `product_is_promote`, `product_is_favorite`, `product_is_delete`, `product_viewed`) VALUES
(1, 0, 1, 0, 'Matrox G200 MMS', '', '', 'Reinforcing its position as a multi-monitor trailblazer, Matrox Graphics Inc. has once again developed the most flexible and highly advanced solution in the industry. Introducing the new Matrox G200 Multi-Monitor Series; the first graphics card ever to support up to four DVI digital flat panel displays on a single 8&quot; PCI board.<br /><br />With continuing demand for digital flat panels in the financial workplace, the Matrox G200 MMS is the ultimate in flexible solutions. The Matrox G200 MMS also supports the new digital video interface (DVI) created by the Digital Display Working Group (DDWG) designed to ease the adoption of digital flat panels. Other configurations include composite video capture ability and onboard TV tuner, making the Matrox G200 MMS the complete solution for business needs.<br /><br />Based on the award-winning MGA-G200 graphics chip, the Matrox G200 Multi-Monitor Series provides superior 2D/3D graphics acceleration to meet the demanding needs of business applications such as real-time stock quotes (Versus), live video feeds (Reuters &amp; Bloombergs), multiple windows applications, word processing, spreadsheets and CAD.', 0, 0, 0, 0, 0, 0),
(2, 0, 1, 0, 'Matrox G400 32MB', '', '', '<strong>Dramatically Different High Performance Graphics</strong><br /><br />Introducing the Millennium G400 Series - a dramatically different, high performance graphics experience. Armed with the industry''s fastest graphics chip, the Millennium G400 Series takes explosive acceleration two steps further by adding unprecedented image quality, along with the most versatile display options for all your 3D, 2D and DVD applications. As the most powerful and innovative tools in your PC''s arsenal, the Millennium G400 Series will not only change the way you see graphics, but will revolutionize the way you use your computer.<br /><br /><strong>Key features:</strong><ul><li>New Matrox G400 256-bit DualBus graphics chip</li><li>Explosive 3D, 2D and DVD performance</li><li>DualHead Display</li><li>Superior DVD and TV output</li><li>3D Environment-Mapped Bump Mapping</li><li>Vibrant Color Quality rendering </li><li>UltraSharp DAC of up to 360 MHz</li><li>3D Rendering Array Processor</li><li>Support for 16 or 32 MB of memory</li></ul>', 0, 0, 0, 0, 0, 0),
(3, 0, 1, 0, 'Microsoft IntelliMouse Pro', '', '', 'Every element of IntelliMouse Pro - from its unique arched shape to the texture of the rubber grip around its base - is the product of extensive customer and ergonomic research. Microsoft''s popular wheel control, which now allows zooming and universal scrolling functions, gives IntelliMouse Pro outstanding comfort and efficiency.', 0, 0, 0, 0, 0, 0),
(4, 0, 1, 0, 'The Replacement Killers', '', '', 'Regional Code: 2 (Japan, Europe, Middle East, South Africa).<br />Languages: English, Deutsch.<br />Subtitles: English, Deutsch, Spanish.<br />Audio: Dolby Surround 5.1.<br />Picture Format: 16:9 Wide-Screen.<br />Length: (approx) 80 minutes.<br />Other: Interactive Menus, Chapter Selection, Subtitles (more languages).', 0, 0, 0, 0, 0, 0),
(5, 0, 1, 0, 'Blade Runner - Director''s Cut', '', '', 'Regional Code: 2 (Japan, Europe, Middle East, South Africa).<br />Languages: English, Deutsch.<br />Subtitles: English, Deutsch, Spanish.<br />Audio: Dolby Surround 5.1.<br />Picture Format: 16:9 Wide-Screen.<br />Length: (approx) 112 minutes.<br />Other: Interactive Menus, Chapter Selection, Subtitles (more languages).', 0, 0, 0, 0, 0, 0),
(6, 0, 1, 0, 'The Matrix', '', '', 'Regional Code: 2 (Japan, Europe, Middle East, South Africa).\r<br />\nLanguages: English, Deutsch.\r<br />\nSubtitles: English, Deutsch.\r<br />\nAudio: Dolby Surround.\r<br />\nPicture Format: 16:9 Wide-Screen.\r<br />\nLength: (approx) 131 minutes.\r<br />\nOther: Interactive Menus, Chapter Selection, Making Of.', 0, 0, 0, 0, 0, 0),
(7, 0, 1, 0, 'You''ve Got Mail', '', '', 'Regional Code: 2 (Japan, Europe, Middle East, South Africa).\r<br />\nLanguages: English, Deutsch, Spanish.\r<br />\nSubtitles: English, Deutsch, Spanish, French, Nordic, Polish.\r<br />\nAudio: Dolby Digital 5.1.\r<br />\nPicture Format: 16:9 Wide-Screen.\r<br />\nLength: (approx) 115 minutes.\r<br />\nOther: Interactive Menus, Chapter Selection, Subtitles (more languages).', 0, 0, 0, 0, 0, 0),
(8, 0, 1, 0, 'A Bug''s Life', '', '', 'Regional Code: 2 (Japan, Europe, Middle East, South Africa).\r<br />\nLanguages: English, Deutsch.\r<br />\nSubtitles: English, Deutsch, Spanish.\r<br />\nAudio: Dolby Digital 5.1 / Dobly Surround Stereo.\r<br />\nPicture Format: 16:9 Wide-Screen.\r<br />\nLength: (approx) 91 minutes.\r<br />\nOther: Interactive Menus, Chapter Selection, Subtitles (more languages).', 0, 0, 0, 0, 0, 0),
(9, 0, 1, 0, 'Under Siege', '', '', 'Regional Code: 2 (Japan, Europe, Middle East, South Africa).\r<br />\nLanguages: English, Deutsch.\r<br />\nSubtitles: English, Deutsch, Spanish.\r<br />\nAudio: Dolby Surround 5.1.\r<br />\nPicture Format: 16:9 Wide-Screen.\r<br />\nLength: (approx) 98 minutes.\r<br />\nOther: Interactive Menus, Chapter Selection, Subtitles (more languages).', 0, 0, 0, 0, 0, 0),
(10, 0, 1, 0, 'Under Siege 2 - Dark Territory', '', '', 'Regional Code: 2 (Japan, Europe, Middle East, South Africa).\r<br />\nLanguages: English, Deutsch.\r<br />\nSubtitles: English, Deutsch, Spanish.\r<br />\nAudio: Dolby Surround 5.1.\r<br />\nPicture Format: 16:9 Wide-Screen.\r<br />\nLength: (approx) 98 minutes.\r<br />\nOther: Interactive Menus, Chapter Selection, Subtitles (more languages).', 0, 0, 0, 0, 0, 0),
(11, 0, 1, 0, 'Fire Down Below', '', '', 'Regional Code: 2 (Japan, Europe, Middle East, South Africa).\r<br />\nLanguages: English, Deutsch.\r<br />\nSubtitles: English, Deutsch, Spanish.\r<br />\nAudio: Dolby Surround 5.1.\r<br />\nPicture Format: 16:9 Wide-Screen.\r<br />\nLength: (approx) 100 minutes.\r<br />\nOther: Interactive Menus, Chapter Selection, Subtitles (more languages).', 0, 0, 0, 0, 0, 0),
(12, 0, 1, 0, 'Die Hard With A Vengeance', '', '', 'Regional Code: 2 (Japan, Europe, Middle East, South Africa).\r<br />\nLanguages: English, Deutsch.\r<br />\nSubtitles: English, Deutsch, Spanish.\r<br />\nAudio: Dolby Surround 5.1.\r<br />\nPicture Format: 16:9 Wide-Screen.\r<br />\nLength: (approx) 122 minutes.\r<br />\nOther: Interactive Menus, Chapter Selection, Subtitles (more languages).', 0, 0, 0, 0, 0, 0),
(13, 0, 1, 0, 'Lethal Weapon', '', '', 'Regional Code: 2 (Japan, Europe, Middle East, South Africa).\r<br />\nLanguages: English, Deutsch.\r<br />\nSubtitles: English, Deutsch, Spanish.\r<br />\nAudio: Dolby Surround 5.1.\r<br />\nPicture Format: 16:9 Wide-Screen.\r<br />\nLength: (approx) 100 minutes.\r<br />\nOther: Interactive Menus, Chapter Selection, Subtitles (more languages).', 0, 0, 0, 0, 0, 0),
(14, 0, 1, 0, 'Red Corner', '', '', 'Regional Code: 2 (Japan, Europe, Middle East, South Africa).\r<br />\nLanguages: English, Deutsch.\r<br />\nSubtitles: English, Deutsch, Spanish.\r<br />\nAudio: Dolby Surround 5.1.\r<br />\nPicture Format: 16:9 Wide-Screen.\r<br />\nLength: (approx) 117 minutes.\r<br />\nOther: Interactive Menus, Chapter Selection, Subtitles (more languages).', 0, 0, 0, 0, 0, 0),
(15, 0, 1, 0, 'Frantic', '', '', 'Regional Code: 2 (Japan, Europe, Middle East, South Africa).\r<br />\nLanguages: English, Deutsch.\r<br />\nSubtitles: English, Deutsch, Spanish.\r<br />\nAudio: Dolby Surround 5.1.\r<br />\nPicture Format: 16:9 Wide-Screen.\r<br />\nLength: (approx) 115 minutes.\r<br />\nOther: Interactive Menus, Chapter Selection, Subtitles (more languages).', 0, 0, 0, 0, 0, 0),
(16, 0, 1, 0, 'Courage Under Fire', '', '', 'Regional Code: 2 (Japan, Europe, Middle East, South Africa).\r<br />\nLanguages: English, Deutsch.\r<br />\nSubtitles: English, Deutsch, Spanish.\r<br />\nAudio: Dolby Surround 5.1.\r<br />\nPicture Format: 16:9 Wide-Screen.\r<br />\nLength: (approx) 112 minutes.\r<br />\nOther: Interactive Menus, Chapter Selection, Subtitles (more languages).', 0, 0, 0, 0, 0, 0),
(17, 0, 1, 0, 'Speed', '', '', 'Regional Code: 2 (Japan, Europe, Middle East, South Africa).\r<br />\nLanguages: English, Deutsch.\r<br />\nSubtitles: English, Deutsch, Spanish.\r<br />\nAudio: Dolby Surround 5.1.\r<br />\nPicture Format: 16:9 Wide-Screen.\r<br />\nLength: (approx) 112 minutes.\r<br />\nOther: Interactive Menus, Chapter Selection, Subtitles (more languages).', 0, 0, 0, 0, 0, 0),
(18, 0, 1, 0, 'Speed 2: Cruise Control', '', '', 'Regional Code: 2 (Japan, Europe, Middle East, South Africa).\r<br />\nLanguages: English, Deutsch.\r<br />\nSubtitles: English, Deutsch, Spanish.\r<br />\nAudio: Dolby Surround 5.1.\r<br />\nPicture Format: 16:9 Wide-Screen.\r<br />\nLength: (approx) 120 minutes.\r<br />\nOther: Interactive Menus, Chapter Selection, Subtitles (more languages).', 0, 0, 0, 0, 0, 1),
(19, 0, 1, 0, 'There''s Something About Mary', '', '', 'Regional Code: 2 (Japan, Europe, Middle East, South Africa).\r<br />\nLanguages: English, Deutsch.\r<br />\nSubtitles: English, Deutsch, Spanish.\r<br />\nAudio: Dolby Surround 5.1.\r<br />\nPicture Format: 16:9 Wide-Screen.\r<br />\nLength: (approx) 114 minutes.\r<br />\nOther: Interactive Menus, Chapter Selection, Subtitles (more languages).', 0, 0, 0, 0, 0, 0),
(20, 0, 1, 0, 'Beloved', '', '', 'Regional Code: 2 (Japan, Europe, Middle East, South Africa).\r<br />\nLanguages: English, Deutsch.\r<br />\nSubtitles: English, Deutsch, Spanish.\r<br />\nAudio: Dolby Surround 5.1.\r<br />\nPicture Format: 16:9 Wide-Screen.\r<br />\nLength: (approx) 164 minutes.\r<br />\nOther: Interactive Menus, Chapter Selection, Subtitles (more languages).', 0, 0, 0, 0, 0, 0),
(21, 0, 1, 0, 'SWAT 3: Close Quarters Battle', '', '', '<strong>Windows 95/98</strong><br /><br />211 in progress with shots fired. Officer down. Armed suspects with hostages. Respond Code 3! Los Angles, 2005, In the next seven days, representatives from every nation around the world will converge on Las Angles to witness the signing of the United Nations Nuclear Abolishment Treaty. The protection of these dignitaries falls on the shoulders of one organization, LAPD SWAT. As part of this elite tactical organization, you and your team have the weapons and all the training necessary to protect, to serve, and "When needed" to use deadly force to keep the peace. It takes more than weapons to make it through each mission. Your arsenal includes C2 charges, flashbangs, tactical grenades. opti-Wand mini-video cameras, and other devices critical to meeting your objectives and keeping your men free of injury. Uncompromised Duty, Honor and Valor!', 0, 0, 0, 0, 0, 0),
(22, 0, 1, 0, 'Unreal Tournament', '', '', 'From the creators of the best-selling Unreal, comes Unreal Tournament. A new kind of single player experience. A ruthless multiplayer revolution.<br /><br />This stand-alone game showcases completely new team-based gameplay, groundbreaking multi-faceted single player action or dynamic multi-player mayhem. It''s a fight to the finish for the title of Unreal Grand Master in the gladiatorial arena. A single player experience like no other! Guide your team of ''bots'' (virtual teamates) against the hardest criminals in the galaxy for the ultimate title - the Unreal Grand Master.', 0, 0, 0, 0, 0, 1),
(23, 0, 1, 0, 'The Wheel Of Time', '', '', 'The world in which The Wheel of Time takes place is lifted directly out of Jordan''s pages; it''s huge and consists of many different environments. How you navigate the world will depend largely on which game - single player or multipayer - you''re playing. The single player experience, with a few exceptions, will see Elayna traversing the world mainly by foot (with a couple notable exceptions). In the multiplayer experience, your character will have more access to travel via Ter''angreal, Portal Stones, and the Ways. However you move around, though, you''ll quickly discover that means of locomotion can easily become the least of the your worries...<br /><br />During your travels, you quickly discover that four locations are crucial to your success in the game. Not surprisingly, these locations are the homes of The Wheel of Time''s main characters. Some of these places are ripped directly from the pages of Jordan''s books, made flesh with Legend''s unparalleled pixel-pushing ways. Other places are specific to the game, conceived and executed with the intent of expanding this game world even further. Either way, they provide a backdrop for some of the most intense first person action and strategy you''ll have this year.', 0, 0, 0, 0, 0, 0),
(24, 0, 1, 0, 'Disciples: Sacred Lands', '', '', 'A new age is dawning...<br /><br />Enter the realm of the Sacred Lands, where the dawn of a New Age has set in motion the most momentous of wars. As the prophecies long foretold, four races now clash with swords and sorcery in a desperate bid to control the destiny of their gods. Take on the quest as a champion of the Empire, the Mountain Clans, the Legions of the Damned, or the Undead Hordes and test your faith in battles of brute force, spellbinding magic and acts of guile. Slay demons, vanquish giants and combat merciless forces of the dead and undead. But to ensure the salvation of your god, the hero within must evolve.<br /><br />The day of reckoning has come... and only the chosen will survive.', 0, 0, 0, 0, 0, 0),
(25, 0, 1, 0, 'Microsoft Internet Keyboard PS/2', '', '', 'The Internet Keyboard has 10 Hot Keys on a comfortable standard keyboard design that also includes a detachable palm rest. The Hot Keys allow you to browse the web, or check e-mail directly from your keyboard. The IntelliType Pro software also allows you to customize your hot keys - make the Internet Keyboard work the way you want it to!', 0, 0, 0, 0, 0, 0),
(26, 0, 1, 0, 'Microsoft IntelliMouse Explorer', '', '', 'Microsoft introduces its most advanced mouse, the IntelliMouse Explorer! IntelliMouse Explorer features a sleek design, an industrial-silver finish, a glowing red underside and taillight, creating a style and look unlike any other mouse. IntelliMouse Explorer combines the accuracy and reliability of Microsoft IntelliEye optical tracking technology, the convenience of two new customizable function buttons, the efficiency of the scrolling wheel and the comfort of expert ergonomic design. All these great features make this the best mouse for the PC!', 0, 0, 0, 0, 0, 0),
(27, 0, 1, 0, 'Hewlett Packard LaserJet 1100Xi', '', '', 'HP has always set the pace in laser printing technology. The new generation HP LaserJet 1100 series sets another impressive pace, delivering a stunning 8 pages per minute print speed. The 600 dpi print resolution with HP''s Resolution Enhancement technology (REt) makes every document more professional.<br /><br />Enhanced print speed and laser quality results are just the beginning. With 2MB standard memory, HP LaserJet 1100xi users will be able to print increasingly complex pages. Memory can be increased to 18MB to tackle even more complex documents with ease. The HP LaserJet 1100xi supports key operating systems including Windows 3.1, 3.11, 95, 98, NT 4.0, OS/2 and DOS. Network compatibility available via the optional HP JetDirect External Print Servers.<br /><br />HP LaserJet 1100xi also features The Document Builder for the Web Era from Trellix Corp. (featuring software to create Web documents).', 0, 0, 0, 0, 0, 0),
(28, 0, 1, 0, 'Samsung Galaxy Tab', '', '', '<p>Powered by a Cortex A8 1.0GHz application processor, the Samsung GALAXY Tab is designed to deliver high performance whenever and wherever you are. At the same time, HD video contents are supported by a wide range of multimedia formats (DivX, XviD, MPEG4, H.263, H.264 and more), which maximizes the joy of entertainment.</p><p>With 3G HSPA connectivity, 802.11n Wi-Fi, and Bluetooth 3.0, the Samsung GALAXY Tab enhances users'' mobile communication on a whole new level. Video conferencing and push email on the large 7-inch display make communication more smooth and efficient. For voice telephony, the Samsung GALAXY Tab turns out to be a perfect speakerphone on the desk, or a mobile phone on the move via Bluetooth headset.</p>', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `fp_products_images`
--

CREATE TABLE IF NOT EXISTS `fp_products_images` (
  `product_id` mediumint(11) NOT NULL,
  `product_image` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  KEY `products_images_prodid` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `fp_products_images`
--

INSERT INTO `fp_products_images` (`product_id`, `product_image`, `sort_order`) VALUES
(28, 'samsung/galaxy_tab_1.jpg', 1),
(28, 'samsung/galaxy_tab_2.jpg', 2),
(28, 'samsung/galaxy_tab_3.jpg', 3),
(28, 'samsung/galaxy_tab_4.jpg', 4),
(17, 'dvd/speed_large.jpg', 1),
(12, 'dvd/die_hard_3_large.jpg', 1),
(11, 'dvd/fire_down_below_large.jpg', 1),
(13, 'dvd/lethal_weapon_large.jpg', 1),
(18, 'dvd/speed_2_large.jpg', 1),
(6, 'dvd/the_matrix_large.jpg', 1),
(4, 'dvd/replacement_killers_large.jpg', 1),
(9, 'dvd/under_siege_large.jpg', 1);

-- --------------------------------------------------------

--
-- 表的结构 `fp_products_os_device`
--

CREATE TABLE IF NOT EXISTS `fp_products_os_device` (
  `os_device_id` mediumint(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `os_device_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `os_device_image` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` int(3) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`os_device_id`),
  KEY `idx_categories_parent_id` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- 转存表中的数据 `fp_products_os_device`
--

INSERT INTO `fp_products_os_device` (`os_device_id`, `parent_id`, `os_device_name`, `os_device_image`, `sort_order`, `date_added`, `last_modified`) VALUES
(1, 0, 'iPhone', 'category_iphone.gif', 1, '2014-09-12 16:26:27', NULL),
(2, 0, 'iPad', 'category_ipad.gif', 2, '2014-09-12 16:26:27', NULL),
(3, 0, 'Mac', 'category_mac.gif', 3, '2014-09-12 16:26:27', NULL),
(4, 0, 'Windows', 'category_windows.gif', 4, '2014-09-12 16:26:27', NULL),
(6, 0, 'Windows Phone', 'category_windows_phone.gif', 5, '2014-09-12 16:26:27', NULL),
(7, 0, 'Android', 'category_android.gif', 6, '2014-09-12 16:26:27', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `fp_products_prices`
--

CREATE TABLE IF NOT EXISTS `fp_products_prices` (
  `product_id` mediumint(11) NOT NULL,
  `currency_id` mediumint(11) NOT NULL,
  `product_price_orginal` decimal(15,4) DEFAULT NULL,
  `product_price_now` decimal(15,4) DEFAULT NULL,
  `sort_order` mediumint(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `fp_products_prices`
--

INSERT INTO `fp_products_prices` (`product_id`, `currency_id`, `product_price_orginal`, `product_price_now`, `sort_order`) VALUES
(1, 2, '19.9900', '12.9900', 1),
(1, 1, '19.9900', '12.9900', 1),
(1, 1, '19.9900', '12.9900', 1),
(1, 2, '19.9900', '12.9900', 1);

-- --------------------------------------------------------

--
-- 表的结构 `fp_products_urls`
--

CREATE TABLE IF NOT EXISTS `fp_products_urls` (
  `product_id` mediumint(11) NOT NULL,
  `product_url` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` mediumint(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `fp_products_urls`
--

INSERT INTO `fp_products_urls` (`product_id`, `product_url`, `sort_order`) VALUES
(1, 'https://itunes.apple.com/us/app/pdf-to-word-pro-by-feiphone/id59', 0),
(1, 'https://itunes.apple.com/us/app/pdf-to-word-pro-by-feiphone/id59', 0),
(2, 'https://itunes.apple.com/us/app/pdf-to-word-pro-by-feiphone/id59', 0);

-- --------------------------------------------------------

--
-- 表的结构 `fp_sessions`
--

CREATE TABLE IF NOT EXISTS `fp_sessions` (
  `sesskey` char(32) NOT NULL DEFAULT '',
  `expiry` int(10) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `adminid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ip` char(15) NOT NULL DEFAULT '',
  `user_name` varchar(60) NOT NULL,
  `discount` decimal(3,2) NOT NULL,
  `email` varchar(60) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`sesskey`),
  KEY `expiry` (`expiry`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `fp_sessions`
--


-- --------------------------------------------------------

--
-- 表的结构 `fp_sessions_data`
--

CREATE TABLE IF NOT EXISTS `fp_sessions_data` (
  `sesskey` varchar(32) NOT NULL DEFAULT '',
  `expiry` int(10) unsigned NOT NULL DEFAULT '0',
  `data` datetime NOT NULL,
  PRIMARY KEY (`sesskey`),
  KEY `expiry` (`expiry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `fp_sessions_data`
--


-- --------------------------------------------------------

--
-- 表的结构 `fp_users`
--

CREATE TABLE IF NOT EXISTS `fp_users` (
  `user_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_roles_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_email` varchar(60) NOT NULL DEFAULT '',
  `user_name` varchar(60) NOT NULL DEFAULT '',
  `user_password` varchar(32) NOT NULL DEFAULT '',
  `user_icon` varchar(255) NOT NULL,
  `user_birthday` date NOT NULL DEFAULT '0000-00-00',
  `user_gender` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `user_country` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `user_address_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_home_phone` varchar(20) NOT NULL DEFAULT '',
  `user_mobile_phone` varchar(20) NOT NULL DEFAULT '',
  `user_money` decimal(10,2) NOT NULL DEFAULT '0.00',
  `user_money_frozen` decimal(10,2) NOT NULL DEFAULT '0.00',
  `user_security_question` varchar(255) NOT NULL,
  `user_security_answer` varchar(255) NOT NULL,
  `user_register_time` datetime NOT NULL,
  `user_register_is_validated` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `user_newsletter` char(1) DEFAULT NULL,
  `user_reset_password_key` varchar(60) NOT NULL DEFAULT '',
  `user_reset_password_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_last_login_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_last_login_ip` varchar(15) NOT NULL,
  `user_visit_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `user_leibie` int(255) NOT NULL COMMENT '类别',
  `user_msn` varchar(255) NOT NULL COMMENT 'MSN',
  `user_qq` int(255) NOT NULL COMMENT 'qq',
  `user_office_phone` varchar(255) NOT NULL COMMENT '办公电话',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  KEY `email` (`user_email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- 转存表中的数据 `fp_users`
--

INSERT INTO `fp_users` (`user_id`, `user_roles_id`, `user_email`, `user_name`, `user_password`, `user_icon`, `user_birthday`, `user_gender`, `user_country`, `user_address_id`, `user_home_phone`, `user_mobile_phone`, `user_money`, `user_money_frozen`, `user_security_question`, `user_security_answer`, `user_register_time`, `user_register_is_validated`, `user_newsletter`, `user_reset_password_key`, `user_reset_password_date`, `user_last_login_time`, `user_last_login_ip`, `user_visit_count`, `user_leibie`, `user_msn`, `user_qq`, `user_office_phone`) VALUES
(21, 0, '8@126.com', '32133213', '25d55ad283aa400af464c76d713c07ad', '', '0000-00-00', 0, 0, 0, '', '', '0.00', '0.00', '', '', '2014-10-14 10:01:04', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 0, '', 0, ''),
(20, 0, '1@126.com', '12212', '25d55ad283aa400af464c76d713c07ad', '', '0000-00-00', 0, 0, 0, '', '', '0.00', '0.00', '', '', '2014-10-14 10:00:22', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 0, '', 0, ''),
(18, 0, '13@126.com', '2313123321312', '25d55ad283aa400af464c76d713c07ad', '', '0000-00-00', 0, 0, 0, '', '', '0.00', '0.00', '', '', '2014-10-14 08:55:01', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 0, '', 0, ''),
(10, 0, '5@126.com', '1122', '25d55ad283aa400af464c76d713c07ad', '', '0000-00-00', 0, 0, 0, '', '', '0.00', '0.00', '', '', '0000-00-00 00:00:00', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 0, '', 0, ''),
(11, 0, '6@126.com', '今日说法', '25d55ad283aa400af464c76d713c07ad', '', '0000-00-00', 0, 0, 0, '', '', '0.00', '0.00', '', '', '0000-00-00 00:00:00', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 0, '', 0, ''),
(12, 0, '7@126.COM', '32424432433', '25d55ad283aa400af464c76d713c07ad', '', '0000-00-00', 0, 0, 0, '', '', '0.00', '0.00', '', '', '0000-00-00 00:00:00', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 0, '', 0, ''),
(13, 0, '8@126.com', '89', '25d55ad283aa400af464c76d713c07ad', '', '0000-00-00', 0, 0, 0, '', '', '0.00', '0.00', '', '', '2014-10-14 00:00:00', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 0, '', 0, ''),
(14, 0, '10@126.com', '12', '25d55ad283aa400af464c76d713c07ad', '', '0000-00-00', 0, 0, 0, '', '', '0.00', '0.00', '', '', '2014-10-14 00:00:00', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 0, '', 0, ''),
(15, 0, '11@126.com', '32354423424', '25d55ad283aa400af464c76d713c07ad', '', '0000-00-00', 0, 0, 0, '', '', '0.00', '0.00', '', '', '2014-10-14 00:00:00', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 0, '', 0, ''),
(16, 0, '12@126.com', 'ssqq', '25d55ad283aa400af464c76d713c07ad', '', '0000-00-00', 0, 0, 0, '', '', '0.00', '0.00', '', '', '2014-10-14 08:48:51', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 0, '', 0, ''),
(17, 0, '13@126.com', '6688', '25d55ad283aa400af464c76d713c07ad', '', '0000-00-00', 0, 0, 0, '', '', '0.00', '0.00', '', '', '2014-10-14 08:52:31', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 0, '', 0, ''),
(34, 0, '88@126.com', '李襄', 'e10adc3949ba59abbe56e057f20f883e', '', '0000-00-00', 0, 0, 0, '', '', '0.00', '0.00', '', '', '2014-10-17 00:59:22', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 0, '', 0, ''),
(32, 0, '8886@126.com', '王96', 'e10adc3949ba59abbe56e057f20f883e', '', '0000-00-00', 0, 0, 0, '', '', '0.00', '0.00', '', '', '2014-10-16 07:48:12', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 0, '', 0, ''),
(35, 0, 'jjkk029@126.com', '李三', 'e10adc3949ba59abbe56e057f20f883e', 'images/human/cloudyfacebook.png', '2014-10-16', 1, 0, 0, '0755-98745867', '15818552711', '0.00', '0.00', '2', '奋斗2', '2014-10-17 07:18:04', 0, NULL, '', '0000-00-00 00:00:00', '2014-10-18 17:48:05', '', 0, 2, '888999', 382783045, '0755-65248794'),
(27, 0, '67@126.com', '22233', '6512bd43d9caa6e02c990b0a82652dca', '', '0000-00-00', 0, 0, 0, '', '', '0.00', '0.00', '', '', '2014-10-16 06:20:53', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 0, '', 0, ''),
(28, 0, '68@126.com', '王2323', 'e10adc3949ba59abbe56e057f20f883e', '', '0000-00-00', 0, 0, 0, '', '', '0.00', '0.00', '', '', '2014-10-16 06:23:29', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 0, '', 0, ''),
(29, 0, '6888@126.com', '李22试试', '79d886010186eb60e3611cd4a5d0bcae', '', '0000-00-00', 0, 0, 0, '', '', '0.00', '0.00', '', '', '2014-10-16 06:49:11', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 0, '', 0, ''),
(30, 0, '688@126.com', '王姗姗', '21218cca77804d2ba1922c33e0151105', '', '0000-00-00', 0, 0, 0, '', '', '0.00', '0.00', '', '', '2014-10-16 06:53:07', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 0, '', 0, ''),
(31, 0, '123456@126.COM', '王99', 'e10adc3949ba59abbe56e057f20f883e', '', '0000-00-00', 0, 0, 0, '', '', '0.00', '0.00', '', '', '2014-10-16 07:31:28', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, 0, '', 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `fp_users_address`
--

CREATE TABLE IF NOT EXISTS `fp_users_address` (
  `address_book_id` int(11) NOT NULL AUTO_INCREMENT,
  `customers_id` int(11) NOT NULL,
  `entry_gender` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_street_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_suburb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entry_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_country_id` int(11) NOT NULL DEFAULT '0',
  `entry_zone_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`address_book_id`),
  KEY `idx_address_book_customers_id` (`customers_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `fp_users_address`
--

INSERT INTO `fp_users_address` (`address_book_id`, `customers_id`, `entry_gender`, `entry_company`, `entry_firstname`, `entry_lastname`, `entry_street_address`, `entry_suburb`, `entry_postcode`, `entry_city`, `entry_state`, `entry_country_id`, `entry_zone_id`) VALUES
(1, 1, 'm', '', 'James', 'Wei', 'asdfavasdf', '', 'asdf', 'asdf', 'asdf', 44, 0);

-- --------------------------------------------------------

--
-- 表的结构 `fp_users_address_format`
--

CREATE TABLE IF NOT EXISTS `fp_users_address_format` (
  `address_format_id` int(11) NOT NULL AUTO_INCREMENT,
  `address_format` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `address_summary` varchar(48) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`address_format_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `fp_users_address_format`
--

INSERT INTO `fp_users_address_format` (`address_format_id`, `address_format`, `address_summary`) VALUES
(1, '$firstname $lastname$cr$streets$cr$city, $postcode$cr$statecomma$country', '$city / $country'),
(2, '$firstname $lastname$cr$streets$cr$city, $state    $postcode$cr$country', '$city, $state / $country'),
(3, '$firstname $lastname$cr$streets$cr$city$cr$postcode - $statecomma$country', '$state / $country'),
(4, '$firstname $lastname$cr$streets$cr$city ($postcode)$cr$country', '$postcode / $country'),
(5, '$firstname $lastname$cr$streets$cr$postcode $city$cr$country', '$city / $country');

-- --------------------------------------------------------

--
-- 表的结构 `fp_users_carts`
--

CREATE TABLE IF NOT EXISTS `fp_users_carts` (
  `user_cart_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_cart_quantily` varchar(255) NOT NULL DEFAULT '',
  `user_cart_add_time` int(10) NOT NULL DEFAULT '0',
  `user_cart_paid_time` int(10) NOT NULL DEFAULT '0',
  `user_cart_user_note` varchar(255) NOT NULL DEFAULT '',
  `user_cart_is_paid` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_cart_id`),
  KEY `user_id` (`user_id`),
  KEY `is_paid` (`user_cart_is_paid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `fp_users_carts`
--


-- --------------------------------------------------------

--
-- 表的结构 `fp_users_roles`
--

CREATE TABLE IF NOT EXISTS `fp_users_roles` (
  `user_role_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `user_role_name` varchar(128) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`user_role_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `fp_users_roles`
--

INSERT INTO `fp_users_roles` (`user_role_id`, `user_parent_id`, `user_role_name`) VALUES
(1, 0, 'individual'),
(2, 0, 'company'),
(3, 0, 'government');

-- --------------------------------------------------------

--
-- 表的结构 `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `sex` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=75 ;

--
-- 转存表中的数据 `test`
--

INSERT INTO `test` (`id`, `name`, `sex`) VALUES
(4, 'hongtenzone', 'M'),
(5, 'hongtenzone', 'M'),
(3, 'hongtenzone', 'M'),
(6, 'hongtenzone', 'M'),
(7, 'hongtenzone', 'M'),
(8, 'hongtenzone', 'M'),
(9, 'hongtenzone', 'M'),
(10, 'hongtenzone', 'M'),
(11, 'hongtenzone', 'M'),
(12, 'hongtenzone', 'M'),
(13, 'hongtenzone', 'M'),
(14, 'hongtenzone', 'M'),
(15, 'hongtenzone', 'M'),
(16, 'hongtenzone', 'M'),
(17, 'hongtenzone', 'M'),
(18, 'hongtenzone', 'M'),
(19, 'hongtenzone', 'M'),
(20, 'hongtenzone', 'M'),
(21, 'hongtenzone', 'M'),
(22, 'hongtenzone', 'M'),
(23, 'hongtenzone', 'M'),
(24, 'hongtenzone', 'M'),
(25, 'hongtenzone', 'M'),
(26, 'hongtenzone', 'M'),
(27, 'hongtenzone', 'M'),
(28, 'hongtenzone', 'M'),
(29, 'M', 'hongtenzone'),
(30, 'hongtenzone', 'M'),
(31, 'hongtenzone', 'M'),
(33, 'hongtenzone', 'M'),
(34, 'hongtenzone', 'M'),
(35, 'hongtenzone', 'M'),
(36, 'hongtenzone', 'M'),
(37, 'hongtenzone', 'M'),
(38, 'hongtenzone', 'M'),
(39, 'hongtenzone', 'M'),
(40, '42525', ''),
(41, '42525', ''),
(42, '42525', ''),
(43, '42525', ''),
(44, '42525', ''),
(45, '42525', ''),
(46, '453535', ''),
(47, '453535', ''),
(48, '88', ''),
(49, '88', ''),
(50, '4erterte@126.com', 'W'),
(51, '4erterte@126.com', 'W'),
(52, '4erterte@126.com', 'W'),
(53, '4erterte@126.com', 'W'),
(54, '', 'W'),
(55, '4erterte@126.com', 'W'),
(56, 'siyecao@126.com', '8888'),
(57, 'siyecao@126.com', '8888'),
(58, '$email', '8888'),
(59, '$email', '8888'),
(60, 'siyecao@126.com', '8888'),
(61, 'siyecao@126.com', '8888'),
(62, 'siyecao@126.com', '8888'),
(63, 'siyecao@126.com', '8899'),
(64, 'siyecao@126.com', '8899'),
(65, 'siyecao@126.com', '8899'),
(66, 'siyecao@126.com', '8899'),
(67, '$email', '8899'),
(68, '4erterte@126.com', '8899'),
(69, '4erterte@126.com', '8899'),
(70, '4erterte@126.com', '889977'),
(71, '4erterte@126.com', '5566'),
(72, '4erterte@126.com', '5566'),
(73, '4erterte@126.com', '5566'),
(74, '4erterte@126.com', '5566');

-- --------------------------------------------------------

--
-- 表的结构 `test88`
--

CREATE TABLE IF NOT EXISTS `test88` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- 转存表中的数据 `test88`
--

INSERT INTO `test88` (`id`, `name`) VALUES
(1, '88'),
(2, '88'),
(3, '9'),
(4, '9'),
(5, '9'),
(6, '9'),
(7, 'jjkk'),
(8, 'jjkk'),
(9, 'jjkk'),
(10, 'siyecao0291986@126.com'),
(11, 'siyecao0291986@126.com'),
(12, 'siyecao0291986@126.com'),
(13, 'siyecao0291986@126.com'),
(14, '4erterte@126.com'),
(15, '4erterte@126.com'),
(16, '4erterte@126.com'),
(17, '4erterte@126.com'),
(18, '4erterte@126.com'),
(19, '4erterte@126.com'),
(20, '4erterte@126.com');
